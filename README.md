# FEM LAB SCRIPTS
This project contains the following Python scripts for the FEM Laboratory:
* *FEMLab_Notebook_1.ipynb*, it's the Jupyter notebook of the 1st exercise. It allows to implement and run the beam model and, then, modify the mesh. To do so, the following 3 scripts are called as subprocesses:
	* *Create_Beam_Model.py* to implement and run the beam model with the given input variables
	* *Read_Beam_Results.py* to read the result of the given job
	* *Modify_Beam_Model.py* to modify the mesh (seeds, geometric order, and integration type) and run

	Each script uses the function of the *Beam_Library.py* specifically created to develop the model of the beam in Abaqus. All input variables are listed in a string and passed to the scripts as system arguments. Each script can be run with or without GUI. If it's run with GUI, the Abaqus window must be closed before running another script with GUI too.

* *FEMLab_Notebook_2.ipynb*, it's the Jupyter notebook of the 2nd exercise. It allows to implement and run the bridge model. To do so, the following script is called as subprocess:
	* *Create_Bridge_Model.py* to implement and run the bridge model with the given input variables which are contained in the following 2 files:
		* *Bridge_nodes.csv*, it collects all the data regarding the nodes of the bridge 
		* *Bridge_beams.csv*, it collects all the data regarding the beams of the bridge

		The paths to these files are given as inputs and passed as system arguments.
	
	The script uses the function of the *Bridge_Library.py* specifically created to develop the model of the bridge in Abaqus. The script can be run with or without GUI. If it's run with GUI, the Abaqus window must be closed before running another script with GUI too.

