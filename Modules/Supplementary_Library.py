# Alessia Ferrara, March 2024

# Imports
import math
import numpy as np
import pandas as pd
import ast
import matplotlib.pyplot as plt
import matplotlib.patches as patches
import plotly.graph_objects as go
import plotly.express as px
from copy import deepcopy

def nice_dataframe_display(data, formats=[]):
# Display the givena dataframe with center alignment, without index column and, if 
# given, set the columns format.
# Args:
# - df = dataframe
# - formats = array of formats
    
    df = deepcopy(data)
    # Set formats, if given
    if formats:
        for i in range(len(formats)):
            value = formats[i]
            df.iloc[:, i] = df.iloc[:, i].apply(value.format)

    # Display dataframe
    df = df.style.set_table_styles([dict(selector='th', props=[('text-align', 'center')])])
    df.set_properties(**{'text-align': 'center'})
    df.hide(axis='index')
    display(df)

    return
# End: def nice_dataframe_display

def sort_mesh(elem_order, elem_integration):
# Sort the element type from given geometric order and integration type.
# Args:
# - elem_order = geometric order
# - elem_integration = integration type

    # Set element type
    if elem_order == 'linear':
        if elem_integration == 'full':
            element = 'C3D8'
        elif elem_integration == 'reduced':
            element = 'C3D8R'
        else:
            raise ValueError('Please set a proper integration type! [full/reduced]')
    elif elem_order == 'quadratic':
        if elem_integration == 'full':
            element = 'C3D20'
        elif elem_integration == 'reduced':
            element = 'C3D20R'
        else:
            raise ValueError('Please set a proper integration type! [full/reduced]')
    else:
        raise ValueError('Please set a proper geometric order! [linear/quadratic]')
    
    return element
# End: def sort_mesh


##### Functions related to beam implementation #####

def display_beam_results(output, ux_t, uz_t):
# Display results from beam simulation.
# Args:
# - output = output array from simulation
# - ux_t = theoretical horizontal displacement
# - uz_t = theoretical vertical displacement

    ux, uz = output[-2], output[-1]
    ux_r, uz_r = ux/ux_t, uz/uz_t     # Calculate relative displacements
    # Create dataframe
    col = ['Element Type', 'Mesh Seeds', 'Ux [m]', 'Uz [m]', 'Ux_r [-]', 'Uz_r [-]']
    df = pd.DataFrame([[output[1], output[5], ux, uz, ux_r, uz_r]], columns=col)
    # Print dataframe
    print("Simulation results:")
    nice_dataframe_display(df)

    return
# End: def display_beam_results

def plot_beam(coordpath, scale_factor=3e7):

    # Read data
    nodes_coord = np.load(coordpath)

    # Sort nodes indexes
    if len(nodes_coord[0][0][0])==8: # linear bricks
        top_idx = [3, 7, 6, 2, 3]
        bottom_idx = [0, 4, 5, 1, 0]
        edges_idx = [[3, 0, 4, 7], [7, 4, 5, 6], [6, 5, 1, 2], [2, 1, 0, 3]]
    elif len(nodes_coord[0][0][0])==20: # quadratic bricks
        top_idx = [0, 16, 4, 15, 7, 19, 3, 11, 0]
        bottom_idx = [1, 17, 5, 13, 6, 18, 2, 9, 1]
        edges_idx = [[0, 8, 1, 17, 5, 12, 4, 16],
                     [4, 12, 5, 13, 6, 14, 7, 15],
                     [7, 14, 6, 18, 2, 10, 3, 19],
                     [3, 10, 2, 9, 1, 8, 0, 11]]

    # Start plot
    fig = go.Figure(
        layout=go.Layout(
        annotations=[
            go.layout.Annotation(
                text='Scale factor = {:.0E}'.format(scale_factor),
                align='left', showarrow=False,
                xref='paper', yref='paper',
                x=0.9, y=0.5,
                bordercolor='black', borderwidth=1)]))

    # Plot undeformed shape
    """for beam in range(len(nodes_coord)): # for each beam
        for el in range(len(nodes_coord[beam])): # for each element
            Node0 = []
            for nod in range(len(nodes_coord[beam][el][0])):
                Node0.append(nodes_coord[beam][el][0][nod]) # initial coord
            Node0 = np.array(Node0)
            fig = draw_beam(fig, Node0, top_idx, bottom_idx, edges_idx,'grey', 'grey', px.colors.qualitative.Pastel2[2])"""

    # Plot deformed shape
    for beam in range(len(nodes_coord)): # for each beam
        for el in range(len(nodes_coord[beam])): # for each element
            # Calculate final position
            Node0 = []
            Node1 = []
            for nod in range(len(nodes_coord[beam][el][0])):
                Node0.append(nodes_coord[beam][el][0][nod]) # initial coord
                U = nodes_coord[beam][el][1][nod] # displacement
                Node1.append([sum(x) for x in zip(Node0[-1], U*scale_factor)]) # final position
            Node1 = np.array(Node1)
            fig = draw_beam(fig, Node1, top_idx, bottom_idx, edges_idx, 'blue', 'blue', px.colors.qualitative.Plotly[0])

    # Set plot layout
    fig.update_layout(template='simple_white', title='Deformation of beam', title_x=0.5,
        scene=dict(
            xaxis=dict(title='X', range=[0,11]),
            yaxis=dict(title='Y', range=[0,11]),
            zaxis=dict(title='Z', range=[0,150]),),
        showlegend=False, width=1000, height=1000)
    fig.update_scenes(zaxis_autorange="reversed")
    fig.layout.scene.camera.projection.type = "orthographic"

    # Show plot
    fig.show()

    return
# End: def plot_beam

def draw_beam(fig, Node, top_idx, bottom_idx, edges_idx, marker_color, line_color, fill_color):

    # Draw nodes
    fig.add_trace(go.Scatter3d(x=Node[:,0], y=Node[:,1], z=Node[:,2],
                               mode='markers', marker=dict(size=1, color=marker_color)))

    # Set connectivity
    if len(Node)==8:
        i = [0, 0]
        j = [1, 2]
        k = [2, 3]
    else:
        i = [0, 1, 3, 5, 5, 1]     
        j = [7, 3, 5, 7, 1, 5]
        k = [1, 2, 4, 6, 7, 3]

    # Draw top
    fig.add_trace(go.Mesh3d(x=Node[top_idx,0], y=Node[top_idx,1], z=Node[top_idx,2],
                               i=i, j=j, k=k, color=fill_color))
    fig.add_trace(go.Scatter3d(
        x=[Node[top_idx,0]], y=[Node[top_idx,1]], z=[Node[top_idx,2]],
        mode='lines', line=dict(color=line_color, width=1)))
    
    # Draw bottom
    fig.add_trace(go.Mesh3d(x=Node[bottom_idx,0], y=Node[bottom_idx,1], z=Node[bottom_idx,2],
                                i=i, j=j, k=k, color=fill_color))
    fig.add_trace(go.Scatter3d(
        x=[Node[bottom_idx,0]], y=[Node[bottom_idx,1]], z=[Node[bottom_idx,2]],
        mode='lines', line=dict(color=line_color, width=1)))
    
    # Draw edges
    for edges in edges_idx:
        fig.add_trace(go.Mesh3d(x=Node[edges,0], y=Node[edges,1], z=Node[edges,2],
                                i=i, j=j, k=k, color=fill_color))
    if len(Node)==8:
        for n in range(len(top_idx)-1):
            fig.add_trace(go.Scatter3d(
                x=[Node[top_idx[n],0], Node[bottom_idx[n],0]],
                y=[Node[top_idx[n],1], Node[bottom_idx[n],1]],
                z=[Node[top_idx[n],2], Node[bottom_idx[n],2]],
                mode='lines', line=dict(color=line_color, width=1)))
    else:
        for edges in edges_idx:
            fig.add_trace(go.Scatter3d(
                x=Node[edges,0], y=Node[edges,1], z=Node[edges,2],
                mode='lines', line=dict(color=line_color, width=1)))


    return fig
# End: def draw_bridge


##### Functions related to bridge implementation #####

def set_bridge_data(nodespath, beamspath, myrand):
# First, reset data of bridge: place node 3 and 5 in the middle between node 1 and 4,
# and node 4 and 6, respectively; apply vertical force on node 4. Then, assign random
# value to cross-section area and Young's modulus.
# Args:
# - nodespath = path to nodes data file
# - beamspath =  path to beams data file

    # Assign random values to force [N], cross-section area [m²] and Young's modulus [Pa]
    Fy = round(-0.5e3+-2e3*myrand[0],1)
    A = round(0.0005+0.0005*myrand[1],6)
    E = int(round(18+6*myrand[2],1)*(10**10))

    # Set nodes dataframe
    nodes = pd.read_csv(nodespath, sep=',', index_col=False) # Load nodes dataframe
    nodes.iloc[:,4] = 0 # Reset vertical force column to zero
    idx = [list(nodes.iloc[:,0]).index(i) for i in [1, 3, 4, 5, 6]]
    nodes.iloc[idx[2],4] = Fy # Apply vertical force [N] to node 4
    nodes.iloc[idx[1],1] = (nodes.iloc[idx[0],1]+nodes.iloc[idx[2],1])/2. # Set position of node 3 as midpoint between node 1 and 4
    nodes.iloc[idx[3],1] = (nodes.iloc[idx[2],1]+nodes.iloc[idx[4],1])/2. # Set position of node 5 as midpoint between node 4 and 6
    nodes.to_csv(nodespath, index=False) # Save nodes dataframe
    
    # Set beams dataframe
    beams = pd.read_csv(beamspath, sep=',', index_col=False) # Load beams dataframe
    beams.iloc[:,3] = A # Assign cross-section area [m²]
    beams.iloc[:,4] = E # Assign Young's modulus [Pa]
    beams.to_csv(beamspath, index=False) # Save beams dataframe

    return nodes, beams
# End: def reset_bridge_data

def change_node_position(nodespath, x):
# Change position of the applied force by varying the position of a node between node 1 and node 6.
# If the new position lies between node 1 and 4, then node 3 will be moved, while if the new position
# lies between node 4 and 6, then node 5 will be moved. The force will be applied to this node. If the
# position corresponds to node 4, no node will be moved and then the force will be applied to node 4.
# Args:
# - nodespath = path to nodes data file
# - x = new position of node n 

    # Create nodes dataframe
    nodes = pd.read_csv(nodespath, sep=',', index_col=False)

    # Check new position
    idx = [list(nodes.iloc[:,0]).index(i) for i in [1, 3, 4, 5, 6]]
    if x < nodes.iloc[idx[0],1] or x > nodes.iloc[idx[4],1]:
        raise ValueError("The new position of node 3 is out of range and can't be assigned."+\
                         " Node 3 must lie between node 1 and 6.")
    elif x >= nodes.iloc[idx[0],1] and x < nodes.iloc[idx[2],1]:
        n = 3
    elif x <= nodes.iloc[idx[4],1] and x > nodes.iloc[idx[2],1]:
        n = 5
    elif x == nodes.iloc[idx[2],1]:
        n = 4
    
    # Apply force to node n
    idx = list(nodes.iloc[:,0]).index(n) # index of node n
    Fy = nodes.iloc[np.nonzero(list(nodes.iloc[:,4]))[0][0],4] # get force value
    nodes.iloc[np.nonzero(list(nodes.iloc[:,4]))[0][0],4] = 0 # reset force column to zero
    nodes.iloc[idx,4] = Fy # apply force to node n
    
    # Change position of node n
    nodes.iloc[idx,1] = x
    
    # Save dataframe
    nodes.to_csv(nodespath, index=False)

    return
# End: def change_node_position

def plot_bridge(coordpath, nodespath, resultspath, scale_factor=3e7):
# Plot bridge undeformed and deformed shape.
# Args:
# - coordpath = path to file where the bridge coordinates and displacements are stored
# - nodespath = path to the nodes data file to get the force value and position
# - resultspath = path to the results file to get the max Von-Mises stress position
# - scale_factor = scale factor to amplify the deformation
    
    # Read nodes data
    nodes = pd.read_csv(nodespath, sep=',', index_col=False)
    # Find force value and position
    idx = np.array(nodes.iloc[:,4]).nonzero()[0][0]
    F = -round(nodes.iloc[idx,4])
    nodeF = [nodes.iloc[idx,1], nodes.iloc[idx,2], 0.]
    
    # Read bridge data
    nodes_coord = np.load(coordpath, allow_pickle=True, encoding='latin1')

    # Sort nodes indexes to draw top, bottom and edges
    if len(nodes_coord[0][0][0])==8: # linear bricks
        top_idx = [3, 7] #, 6, 2, 3]
        bottom_idx = [0, 4]#, 5, 1, 0]
        edges_idx = [3, 0, 4, 7] #, [7, 4, 5, 6], [6, 5, 1, 2], [2, 1, 0, 3]]
    elif len(nodes_coord[0][0][0])==20: # quadratic bricks
        top_idx = [0, 16, 4] #, 15, 7, 19, 3, 11, 0]
        bottom_idx = [1, 17, 5] #, 13, 6, 18, 2, 9, 1]
        edges_idx = [0, 8, 1, 17, 5, 12, 4, 16] #, [4, 12, 5, 13, 6, 14, 7, 15], [7, 14, 6, 18, 2, 10, 3, 19], [3, 10, 2, 9, 1, 8, 0, 11]]
    
    # Start plot
    fig, ax = plt.subplots(figsize=(12, 8))
    
    # Plot undeformed shape
    idxF = []
    for beam in range(len(nodes_coord)): # for each beam
        for el in range(len(nodes_coord[beam])): # for each element
            Node0 = []
            for nod in range(len(nodes_coord[beam][el][0])):
                Node0.append(nodes_coord[beam][el][0][nod]) # initial coord
            Node0 = np.array(Node0)
            # Check initial force position
            coord, nod = closest_coordinate(nodeF, Node0)
            idxF.append([beam, el, 0, nod, coord])
            draw_bridge(ax, Node0, top_idx, bottom_idx, edges_idx, 'silver', 'whitesmoke')
    # Get initial force position
    coord, i = closest_coordinate(nodeF, [row[4] for row in idxF])
    F0 = idxF[i][:4]

    # Mark max Von Mises stress position
    df = pd.read_csv(resultspath, index_col=False, delimiter='\t')
    VM = ast.literal_eval(df.iloc[-1]['Max_VonMises_position'])
    plt.scatter(VM[0], VM[1], marker='*', color='magenta', s=200)
    ax.text(-1.5, 1.8, ' = max Von Mises', fontsize=10, ha='center', va='center', color='black')
    ax.text(-1.8, 1.8, '\u2605', fontsize=18, ha='center', va='center', color='magenta')

    # Plot deformed shape
    for beam in range(len(nodes_coord)): # for each beam
        for el in range(len(nodes_coord[beam])): # for each element
            # Calculate final position
            Node0 = []
            Node1 = []
            for nod in range(len(nodes_coord[beam][el][0])):
                Node0.append(nodes_coord[beam][el][0][nod]) # initial coord
                U = nodes_coord[beam][el][1][nod] # displacement
                Node1.append([sum(x) for x in zip(Node0[-1], U*scale_factor)]) # final position
                # Find final force position
                if [beam, el, 0, nod] == F0:
                    Fx = Node1[-1][0]
                    Fy = Node1[-1][1]
            Node0 = np.array(Node0)
            Node1 = np.array(Node1)
            draw_bridge(ax, Node1, top_idx, bottom_idx, edges_idx, 'blue', 'lightskyblue')

    # Plot force arrow
    plt.arrow(Fx, (Fy+0.4), Fx - Fx, (Fy+0.03) - (Fy+0.4), 
          width=0.005, head_width=0.03, head_length=0.03, fc='red', ec='red') # plot arrow
    plt.text(Fx+0.02, Fy+0.3, '{} N'.format(F), fontsize=14, color='red') # set force value textbox
    
    # Plot general settings
    plt.text(1.7, 1.8, 'Scale factor = {:,.1e}'.format(scale_factor), fontsize=10, color='black',
             ha='center', va='center', bbox=dict(facecolor='white', edgecolor='black', boxstyle='square')) # set scale factor textbox
    plt.gca().spines['top'].set_visible(False) # hide top spine
    plt.gca().spines['right'].set_visible(False) # hide right spine
    ax.set_xlim(-2, 2)
    ax.set_ylim(-0.1, 2)
    plt.title('Deformation of bridge under concentrated force') # set title
    plt.xlabel('X') # set X axis label
    plt.ylabel('Y') # set Y axis label

    plt.show()

    return
# End: plot_bridge

def draw_bridge(ax, Node, top_idx, bottom_idx, edges_idx, line_color, face_color):
# Draw the bridge.

    linewidth = 1
    """# Draw top
    for n in range(len(top_idx)-1):
        plt.plot([Node[top_idx[n],0], Node[top_idx[n+1],0]], [Node[top_idx[n],1], Node[top_idx[n+1],1]],
                 color=line_color, linestyle='-', linewidth=linewidth)
    # Draw bottom
    for n in range(len(bottom_idx)-1):
        plt.plot([Node[bottom_idx[n],0], Node[bottom_idx[n+1],0]], [Node[bottom_idx[n],1], Node[bottom_idx[n+1],1]],
                 color=line_color, linestyle='-', linewidth=linewidth)
    
    # Draw sides
    for n in range(len(edges_idx)-1):
        plt.plot([Node[edges_idx[n],0], Node[edges_idx[n+1],0]], [Node[edges_idx[n],1], Node[edges_idx[n+1],1]],
                 color=line_color, linestyle='-', linewidth=linewidth)"""
    
    vertices = []
    for n in edges_idx:
        vertices.append([Node[n,0], Node[n,1]])
    polygon = patches.Polygon(vertices, closed=True, lw = linewidth, edgecolor=line_color, facecolor=face_color)
    ax.add_patch(polygon)

    return
# End: draw_bridge

def euclidean_distance(coord1, coord2):
# Calculate the 3D euclidean distance between 2 nodes.
# Args:
# - coord1 = coordinates of node 1
# - coord2 = coordinates of node 2

    dist = math.sqrt((coord1[0] - coord2[0])**2 + (coord1[1] - coord2[1])**2 + (coord1[2] - coord2[2])**2)

    return dist
# End: euclidean_distance

def closest_coordinate(node, coordinates):
# Find the set of coordinates closest to the given node.
# Args:
# - node = node to find the closest coordinates to
# coordinates = array of coordinates

    closest_index = None
    closest_coord = None
    min_distance = float('inf')
    
    for index, coord in enumerate(coordinates):
        distance = euclidean_distance(node, coord)
        if distance < min_distance:
            min_distance = distance
            closest_index = index
            closest_coord = coord
    
    return closest_coord, closest_index
# End: closest_coordinate

def plot_bridge_plotly(coordpath, nodespath, resultspath, scale_factor=3e7):
# Plot bridge undeformed and deformed shape.
# Args:
# - coordpath = path to file where the bridge coordinates and displacements are stored
# - nodespath = path to the nodes data file to get the force value and position
# - scale_factor = scale factor to amplify the deformation
    
    # Read nodes data
    nodes = pd.read_csv(nodespath, sep=',', index_col=False)
    # Find force value and position
    idx = np.array(nodes.iloc[:,4]).nonzero()[0][0]
    F = -round(nodes.iloc[idx,4])
    nodeF = [nodes.iloc[idx,1], nodes.iloc[idx,2], 0.]
    
    # Read bridge data
    nodes_coord = np.load(coordpath, allow_pickle=True, encoding='latin1')

    # Sort nodes indexes to draw top, bottom and edges
    if len(nodes_coord[0][0][0])==8: # linear bricks
        top_idx = [3, 7, 6, 2, 3]
        bottom_idx = [0, 4, 5, 1, 0]
        edges_idx = [[3, 0, 4, 7], [7, 4, 5, 6], [6, 5, 1, 2], [2, 1, 0, 3]]
    elif len(nodes_coord[0][0][0])==20: # quadratic bricks
        top_idx = [0, 16, 4, 15, 7, 19, 3, 11, 0]
        bottom_idx = [1, 17, 5, 13, 6, 18, 2, 9, 1]
        edges_idx = [[0, 8, 1, 17, 5, 12, 4, 16],
                     [4, 12, 5, 13, 6, 14, 7, 15],
                     [7, 14, 6, 18, 2, 10, 3, 19],
                     [3, 10, 2, 9, 1, 8, 0, 11]]

    # Start plot
    fig = go.Figure(layout=go.Layout(annotations=[go.layout.Annotation(
                text='Scale factor = {:.0E}'.format(scale_factor),
                align='left', showarrow=False, xref='paper', yref='paper', x=0.95, y=1.05,
                bordercolor='black', borderwidth=1)]))

    # Plot undeformed shape
    idxF = []
    for beam in range(len(nodes_coord)): # for each beam
        for el in range(len(nodes_coord[beam])): # for each element
            Node0 = []
            for nod in range(len(nodes_coord[beam][el][0])):
                Node0.append(nodes_coord[beam][el][0][nod]) # initial coord
            Node0 = np.array(Node0)
            # Check initial force position
            coord, nod = closest_coordinate(nodeF, Node0)
            idxF.append([beam, el, 0, nod, coord])
            fig = draw_bridge(fig, Node0, top_idx, bottom_idx, edges_idx, 'grey', 'grey', px.colors.qualitative.Pastel2[2])
    # Get initial force position
    coord, i = closest_coordinate(nodeF, [row[4] for row in idxF])
    F0 = idxF[i][:4]

    # Plot deformed shape
    for beam in range(len(nodes_coord)): # for each beam
        for el in range(len(nodes_coord[beam])): # for each element
            # Calculate final position
            Node0 = []
            Node1 = []
            for nod in range(len(nodes_coord[beam][el][0])):
                Node0.append(nodes_coord[beam][el][0][nod]) # initial coord
                U = nodes_coord[beam][el][1][nod] # displacement
                Node1.append([sum(x) for x in zip(Node0[-1], U*scale_factor)]) # final position
                # Find final force position
                if [beam, el, 0, nod] == F0:
                    Fx = Node1[-1][0]
                    Fy = Node1[-1][1]
            Node0 = np.array(Node0)
            Node1 = np.array(Node1)
            fig = draw_bridge(fig, Node1, top_idx, bottom_idx, edges_idx, 'blue', 'blue', px.colors.qualitative.Plotly[0])
    
    # Mark max Von Mises stress position
    df = pd.read_csv(resultspath, index_col=False, delimiter='\t')
    VM = ast.literal_eval(df.iloc[-1]['Max_VonMises_position'])
    fig.add_trace(go.Scatter(x=[VM[0]], y=[VM[1]],
            marker= dict(size=14, symbol='star', color='cyan')))
    annotations = dict(text='<span style="color: cyan;">\u2605</span> <span style="color: black;"> = max Von-Mises</span>',   
                align='left', showarrow=False, xref='paper', yref='paper', x=0.05, y=1.05,
                bordercolor='black', borderwidth=1)
    fig.add_annotation(annotations)

    # Draw force arrow
    fig.add_trace(go.Scatter(x=[Fx,Fx], y=[Fy+0.5,Fy],
            marker= dict(size=8, symbol='arrow-bar-up', angleref='previous', color='red')))
    annotations = dict(x=Fx+0.2, y=Fy+0.45, font=dict(color='red', size=16),
        xref="x", yref="y", text=str(F) + ' N',
        showarrow=False, bordercolor="rgba(0,0,0,0)",)
    fig.add_annotation(annotations)

    # Set plot layout
    fig.update_layout(template='simple_white',
        xaxis=dict(title='X'), yaxis=dict(title='Y'),
        title='Deformation of bridge under concentrated force', title_x=0.5,
        showlegend=False, width=800, height=600)
    

    # Show plot
    fig.show()

    return
# End: plot_bridge

def draw_bridge_plotly(fig, Node, top_idx, bottom_idx, edges_idx, marker_color, line_color, fill_color):
# Draw the bridge in 

    # Draw nodes
    fig.add_trace(go.Scatter(x=Node[:,0], y=Node[:,1],
            mode='markers', marker=dict(size=1, color=marker_color)))
    # Draw top
    for n in range(len(top_idx)-1):
        fig.add_trace(go.Scatter(
            x=[Node[top_idx[n],0], Node[top_idx[n+1],0]],
            y=[Node[top_idx[n],1], Node[top_idx[n+1],1]],
            mode='lines', line=dict(color=line_color, width=1)))#, fill='tonexty', fillcolor=fill_color))
    # Draw bottom
    for n in range(len(bottom_idx)-1):
        fig.add_trace(go.Scatter(
            x=[Node[bottom_idx[n],0], Node[bottom_idx[n+1],0]],
            y=[Node[bottom_idx[n],1], Node[bottom_idx[n+1],1]],
            mode='lines', line=dict(color=line_color, width=1)))#, fill='tonexty', fillcolor=fill_color))
    
    # Draw edges
    if len(Node)==8:
        for n in range(len(top_idx)-1):
            fig.add_trace(go.Scatter(
                x=[Node[top_idx[n],0], Node[bottom_idx[n],0]],
                y=[Node[top_idx[n],1], Node[bottom_idx[n],1]],
                mode='lines', line=dict(color=line_color, width=1)))#, fill='tonexty', fillcolor=fill_color))
    else:
        for edges in edges_idx:
            fig.add_trace(go.Scatter(
                x=Node[edges,0], y=Node[edges,1],
                mode='lines', line=dict(color=line_color, width=1)))#, fill='tonexty', fillcolor=fill_color))

    return fig
# End: draw_bridge








