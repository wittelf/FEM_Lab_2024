
# Imports
import os
import imp
import sys

# Set index to read inputs
execMode = sys.argv[-1]
if execMode == 'noGUI':
    idx = 10
elif execMode == 'script':
    idx = 8
else:
    raise ValueError('Pleas, set the correct execution mode: script or noGUI.')

# Set working directory
libPath = sys.argv[idx] 
setPath =  r'{}'.format(libPath)
os.chdir(setPath)

# Import beam library
import Beam_Library as beam
imp.reload(beam)
from Beam_Library import *

# Set input variables
jobName = sys.argv[idx+1] # job name
Nx = int(sys.argv[idx+2]) # number of horizontal elements
Nz = int(sys.argv[idx+3]) # number of vertical elements
elemType = sys.argv[idx+4] # element type

# Read and save results
data = beam.get_displacements(jobName, Nx, Nz, elemType)
print >> sys.__stdout__, data

