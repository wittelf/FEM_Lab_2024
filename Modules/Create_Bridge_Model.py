
# Imports
import os
import sys
import numpy as np

# Set index to read inputs
execMode = sys.argv[-1]
if execMode == 'noGUI':
    idx = 10
elif execMode == 'script':
    idx = 8
else:
    raise ValueError('Please, set the correct execution mode: script or noGUI.')

# Set working directory
libPath = sys.argv[idx]
print(libPath)
setPath =  r'{}'.format(libPath)
os.chdir(setPath)

# Import bridge library
import imp
import Bridge_Library as bridge
imp.reload(bridge)
from Bridge_Library import *

# Set input variables
# Cae path
caePath = sys.argv[idx+1] # cae file path
# Bridge data
nodespath = sys.argv[idx+2] # path to nodes data file
nodes = np.loadtxt(open(nodespath, "rb"), delimiter=",", skiprows=1) # nodes data
beamspath = sys.argv[idx+3] # path to beam data file
beams = np.loadtxt(open(beamspath, "rb"), delimiter=",", skiprows=1) # beams data
# Check beams -> if there are beams with nodes that have the same coordinates, 
# then cut them out
nb = len(beams)
cut_idx = []
for k in range(nb):
    i, = np.where(nodes[:,0]==beams[k,1])[0]
    p0 = [nodes[i][1], nodes[i][2]]
    i, = np.where(nodes[:,0]==beams[k,2])[0]
    p1 = [nodes[i][1], nodes[i][2]]
    if p0[0]==p1[0] and p0[1]==p1[1]:
        cut_idx.append(k)
beams = np.delete(beams, cut_idx, axis=0)
# Mesh parameters
numElem = float(sys.argv[idx+4]) # number of elements along the beam
elemType = sys.argv[idx+5] # integration type (full or reduced)
jobName = sys.argv[idx+6] # job name
resultsPath = sys.argv[idx+7] # save file path
coordPath = sys.argv[idx+8] # save file path

# Create bridge model
# Create part
modelName = bridge.create_bridge_part(nodes, beams)
# Create and assign material
bridge.create_bridge_material(modelName, beams)
# Create assembly
bridge.create_bridge_assembly(modelName, nodes, beams)
# Create static step
stepName = bridge.create_static_step(modelName)
# Create mesh
setName = bridge.create_bridge_mesh(modelName, nodes, beams, numElem, elemType)
# Create boundary conditions
bridge.create_boundary_conditions(modelName, stepName, nodes, beams)
# Create loads
bridge.create_loads(modelName, stepName, nodes, beams)
# Save model
bridge.save_model(caePath)
print >> sys.__stdout__, "The model has been saved to " + caePath + " ."
# Create and submit job
bridge.submit_job(modelName, jobName)
print >> sys.__stdout__, "Job " + jobName + " completed."
# Save results
bridge.get_results(jobName, nodes, elemType, numElem, resultsPath)
bridge.get_elements_nodes(jobName, coordPath)

