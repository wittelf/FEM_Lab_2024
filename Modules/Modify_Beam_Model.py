
# Imports
import os
import imp
import sys

# Set index to read inputs
execMode = sys.argv[-1]
if execMode == 'noGUI':
    idx = 10
elif execMode == 'script':
    idx = 8
else:
    raise ValueError('Pleas, set the correct execution mode: script or noGUI.')

# Set working directory
libPath = sys.argv[idx]
print(libPath)
setPath =  r'{}'.format(libPath)
os.chdir(setPath)

# Import beam library
import Beam_Library as beam
imp.reload(beam)
from Beam_Library import *

# Set input variables
# Cae path
caePath = sys.argv[idx+1] # cae file path
# Geometry variables
l = float(sys.argv[idx+2]) # z [m]
b = float(sys.argv[idx+3]) # y [m]
h = float(sys.argv[idx+4]) # x [m]
# Mesh parameters
numElem_vert = int(sys.argv[idx+5]) # number of elements along the beam
numElem_hor = int(sys.argv[idx+6]) # number of elements along the beam
elemType = sys.argv[idx+7] # element type
jobName = sys.argv[idx+8] # job name
filePath = sys.argv[idx+9] # save file path

# Modify beam model
# Open saved model
beam.open_model(caePath)
# Get model name and part name 
modelName, partName = beam.get_model_names()
# Create new mesh
delete_mesh(modelName, partName)
beam.create_local_seeds(modelName, partName, l, b, h, numElem_vert, numElem_hor)
beam.create_mesh(modelName, partName, b, h, numElem_hor)
beam.set_mesh_element_type(modelName, partName, elemType)
# Create and submit job
beam.submit_job(modelName, jobName)
# Save results
data = beam.get_displacements(jobName, numElem_hor, numElem_vert, elemType, filePath)
#print >> sys.__stdout__, "Job " + jobName + " completed."