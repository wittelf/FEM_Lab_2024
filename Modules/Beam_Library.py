# Alessia Ferrara, February 2024

# Imports
from abaqus import *
from caeModules import *
from abaqusConstants import *
from visualization import *
import glob
import os.path
import numpy as np

def create_beam_part(l, b, h, partName=''):
# Create beam part.
# Args:
# - l = length of the beam (z)
# - b = thickness of the beam (y)
# - h = width of the beam (x)
# Returns:
# - modelName = name of the model
# - partName = name of the part
    
    mdb = Mdb()
    modelName = 'Model-1'
    if partName == '':
        partName = 'Beam'
    myModel = mdb.models[modelName]

    # Create part
    myModel.Part(name=partName, dimensionality=THREE_D, type=DEFORMABLE_BODY)
    myPart = myModel.parts[partName]          
    mySketch = myModel.ConstrainedSketch(name='__profile__', sheetSize=20.0)
    mySketch.rectangle(point1=(0.0, 0.0), point2=(h, b))
    myPart.BaseSolidExtrude(sketch=mySketch, depth=l)
    del myModel.sketches['__profile__']
    session.viewports['Viewport: 1'].setValues(displayedObject=myPart)   

    return modelName, partName
# End: def create_beam_part

def create_isotropic_material(modelName, partName, Emod, Pratio, materialName=''):
# Create and assign an isotropic material.
# Args:
# - Emod = Young's modulus
# - Pratio = Poisson's ratio

    myModel = mdb.models[modelName]
    myPart = myModel.parts[partName]
    if materialName == '':
        materialName = 'Material'

    # Create material
    myModel.Material(name=materialName)
    myMaterial = myModel.materials[materialName]
    myMaterial.Elastic(type=ISOTROPIC, table=((Emod,Pratio), ))
    # Create section
    sectionName = materialName
    myModel.HomogeneousSolidSection(name=sectionName, material=materialName, thickness=None)
    # Assign section
    setName = 'Set-'+partName
    myPart.Set(cells=myPart.cells, name=setName)
    myPart.SectionAssignment(offset=0.0, offsetField='', offsetType=MIDDLE_SURFACE,
        region=myPart.sets[setName], sectionName=sectionName, thicknessAssignment=FROM_SECTION)

    return materialName
# End: def create_isotropic_material

def create_beam_assembly(modelName, partName):
# Create beam assembly.
# Args: none

    myModel = mdb.models[modelName]
    myPart = myModel.parts[partName]

    # Create assembly
    myAssembly = myModel.rootAssembly
    myAssembly.Instance(name=partName, part=myPart, dependent=ON)

    return
# End: def create_beam_assembly

def create_static_step(modelName, stepName='', nlgeom=OFF, timePeriod=1., previous="Initial",
                        initialInc=0.1, maxNumInc=100, minInc=1e-5, maxInc=1):
# Create static step.
# Args: optional according to Abaqus nomenclature
    
    myModel = mdb.models[modelName]
    if stepName == '':
        stepName = 'Step-1'
    
    # Create step
    myModel.StaticStep(name=stepName, timePeriod=timePeriod, nlgeom=nlgeom, previous=previous,
                        initialInc=initialInc, maxNumInc=maxNumInc, minInc=minInc, maxInc=maxInc)
    
    return stepName
# End: create_static_step

def create_boundary_conditions(modelName, stepName, partName, l, b, h):
# Create encastre at the bottom face.
# Args: none
    
    myModel = mdb.models[modelName]
    myAssembly = myModel.rootAssembly
    myInstance = myAssembly.instances[partName]

    # Create face set
    face = myInstance.faces.findAt(((h/2,b/2.,l),),)
    setName = 'Bottom-Face'
    myRegion = myAssembly.Set(faces=face, name=setName)

    # Set encastre
    boundName = 'Encastre'
    myModel.EncastreBC(name=boundName, createStepName=stepName, 
        region=myRegion, localCsys=None)

    return
# End: def create_boundary_conditions

def create_loads(modelName, stepName, partName, b, h, Fx, Fz):
# Set the boundary consitions (BC).
# Args:
# - Fx = 
# - Fz = 
    
    myModel = mdb.models[modelName]
    myAssembly = myModel.rootAssembly
    myInstance = myAssembly.instances[partName]

    # Create vertices set
    points = myInstance.vertices.findAt(((0.,0.,0.),), ((0.,b,0.),),
                                        ((h,0.,0.),), ((h,b,0.),),)
    setName = 'Top-Vertices'
    myRegion = myAssembly.Set(vertices=points, name=setName)

    # Create loads
    loadName = 'Loads'
    myModel.ConcentratedForce(name=loadName, createStepName=stepName, 
        region=myRegion, cf1=Fx/4., cf3=Fz/4., distributionType=UNIFORM, field='', 
        localCsys=None)
    
    return setName
# End: def create_loads

def create_global_seeds(modelName, partName, global_size, minSizeFactor=0.1, deviationFactor=0.1):
#
#

    myModel = mdb.models[modelName]
    myPart = myModel.parts[partName]

    # Seed part
    myPart.seedPart(deviationFactor=deviationFactor, minSizeFactor=minSizeFactor, size=global_size)   

    return
# End: def create_global_seeds

def create_local_seeds(modelName, partName, l, b, h, num_el_vert, num_el_hor):
#
#
    
    myModel = mdb.models[modelName]
    myPart = myModel.parts[partName]

    # Create edges sets
    edges = myPart.edges.findAt(((0.,0.,l/2.),), ((0.,b,l/2.),),
                                    ((h,0.,l/2.),), ((h,b,l/2.),),)
    myPart.seedEdgeByNumber(edges=edges, number=num_el_vert, constraint=FINER)

    edges = myPart.edges.findAt(((0.,b/2.,0.),), ((h,b/2.,0.),),
                                    ((h/2.,0.,0.),), ((h/2.,b,0.),),
                                    ((0.,b/2.,l),), ((h,b/2.,l),),
                                    ((h/2.,0.,l),), ((h/2.,b,l),),)
    myPart.seedEdgeByNumber(edges=edges, number=num_el_hor, constraint=FINER)

    return
# End: def create_local_seeds

def create_mesh(modelName, partName, b, h, num_el_hor):
#
#
    
    myModel = mdb.models[modelName]
    myPart = myModel.parts[partName]
    myAssembly = myModel.rootAssembly
    instanceName = myAssembly.instances.keys()[0]
    myInstance = myAssembly.instances[instanceName]

    # Generate mesh
    myPart.generateMesh()
    myAssembly.regenerate()

    # Set outputs
    allNodes = myInstance.nodes
    delta = 1e-3
    if (num_el_hor%2)==0:
        myNodes = allNodes.getByBoundingBox(h/2.-delta, b/2.-delta, 0.-delta, h/2.+delta, b/2.+delta, 0.+delta)
    else:
        myNodes = allNodes.getByBoundingBox(h/2.-h/(num_el_hor*2.)-delta, b/2.-b/(num_el_hor*2.)-delta, 0.-delta, h/2.-h/(num_el_hor*2.)+delta, b/2.-b/(num_el_hor*2.)+delta, 0.+delta
                  ) + allNodes.getByBoundingBox(h/2.-h/(num_el_hor*2.)-delta, b/2.+b/(num_el_hor*2.)-delta, 0.-delta, h/2.-h/(num_el_hor*2.)+delta, 
                  b/2.+b/(num_el_hor*2.)+delta, 0.+delta) + allNodes.getByBoundingBox(h/2.+h/(num_el_hor*2.)-delta, 
                  b/2.-b/(num_el_hor*2.)-delta, 0.-delta, h/2.+h/(num_el_hor*2.)+delta, b/2.-b/(num_el_hor*2.)+delta, 0.+delta
                  ) + allNodes.getByBoundingBox(h/2.+h/(num_el_hor*2.)-delta, b/2.+b/(num_el_hor*2.)-delta, 0.-delta, h/2.+h/(num_el_hor*2.)+delta, b/2.+b/(num_el_hor*2.)+delta, 0.+delta)
  
    # Set output request
    stepName = myModel.steps.keys()[1]
    # Create history output (U1, U3)
    setName = 'Read-Disp'
    myRegion = myAssembly.Set(nodes=myNodes, name=setName)
    myModel.HistoryOutputRequest(name='Beam-HO', createStepName=stepName, variables=('U1', 'U3'), 
                                 region=myRegion, sectionPoints=DEFAULT, rebar=EXCLUDE)
    # Create field output (COORD)
    myModel.FieldOutputRequest(name='Beam-FO', createStepName=stepName, variables=('COORD',),
                               region=MODEL, sectionPoints=DEFAULT, position=INTEGRATION_POINTS, rebar=EXCLUDE)

    return
# End: def create_mesh

def delete_mesh(modelName, partName):
# Delete existing mesh.
# # Args: none
    
    myModel = mdb.models[modelName]
    myPart = myModel.parts[partName]

    # Delete existing mesh
    myPart.deleteMesh()
    myPart.deleteSeeds()

    return
# End: def delete_mesh

def set_mesh_element_type(modelName, partName, elemtype='C3D20R'):
# Set element type.
# Args:
# - modelName = name of the model
# - partName = name of the part
# - elemtype = element type (C3D8, C3D8R, C3D20, C3D20R)

    myModel = mdb.models[modelName]
    myPart = myModel.parts[partName]

    if elemtype == 'C3D8':
        # Linear + full integration
        elemType1 = mesh.ElemType(elemCode=C3D8, elemLibrary=STANDARD,
                                    secondOrderAccuracy=OFF, distortionControl=DEFAULT)
        elemType2 = mesh.ElemType(elemCode=C3D6, elemLibrary=STANDARD)
        elemType3 = mesh.ElemType(elemCode=C3D4, elemLibrary=STANDARD)
    elif elemtype == 'C3D8R':
        # Linear + reduced integration
        elemType1 = mesh.ElemType(elemCode=C3D8R, elemLibrary=STANDARD,
                                  kinematicSplit=AVERAGE_STRAIN, secondOrderAccuracy=OFF,
                                  hourglassControl=DEFAULT, distortionControl=DEFAULT)
        elemType2 = mesh.ElemType(elemCode=C3D6, elemLibrary=STANDARD)
        elemType3 = mesh.ElemType(elemCode=C3D4, elemLibrary=STANDARD)
    elif elemtype == 'C3D20':
        # Quadratic + full integration
        elemType1 = mesh.ElemType(elemCode=C3D20, elemLibrary=STANDARD)
        elemType2 = mesh.ElemType(elemCode=C3D15, elemLibrary=STANDARD)
        elemType3 = mesh.ElemType(elemCode=C3D10, elemLibrary=STANDARD)
    elif elemtype == 'C3D20R':
        # Quadratic + reduced integration
        elemType1 = mesh.ElemType(elemCode=C3D20R, elemLibrary=STANDARD)
        elemType2 = mesh.ElemType(elemCode=C3D15, elemLibrary=STANDARD)
        elemType3 = mesh.ElemType(elemCode=C3D10, elemLibrary=STANDARD)
    else:
        raise ValueError('Unknown element type')

    # Set element type
    myPart.setElementType(elemTypes=(elemType1,elemType2,elemType3), regions=(myPart.cells, ))

    return
# End: def change_mesh_element_type

def submit_job(modelName, jobName=''):
# Create and submit job.
# Args:
# - jobName = name of the job
# - modelName = name of the model
    
    # Remove existing .lck file
    fileExt = '.lck'
    filePath = os.getcwd() + '/' + jobName + fileExt
    if os.path.exists(filePath):
        os.remove(filePath)

    # Create job
    if jobName == '':
        jobName = 'Job-1'
    mdb.Job(name=jobName, model = modelName, type=ANALYSIS, atTime=None, contactPrint=OFF, description='', echoPrint=OFF,
        explicitPrecision=SINGLE, getMemoryFromAnalysis=True, historyPrint=OFF, memory=90, memoryUnits=PERCENTAGE, 
        modelPrint=OFF, multiprocessingMode=DEFAULT, nodalOutputPrecision=SINGLE, numCpus=1, queue=None, scratch='', 
        userSubroutine='', waitHours=0, waitMinutes=0)
    
    # Run job
    myJob = mdb.jobs[jobName]
    myJob.submit(consistencyChecking=OFF)
    myJob.waitForCompletion()

    # Remove simulation files except odb
    path_all = glob.glob(os.getcwd() + '/' + jobName + '.*')
    extension = ('.odb','.simdir','.log') #, '.res', '.prt', '.mdl', '.stt') # extension to keep
    path_remove = [filename for filename in path_all if not filename.endswith(extension)]
    [os.remove(filePath) for filePath in path_remove]

    """# Remove abaqusa.rpy files
    path_remove = glob.glob(os.path.dirname(os.getcwd()) + '/' + 'abaqus.rpy*')
    [os.remove(filePath) for filePath in path_remove]"""
        
    return
# End: def submit_job

def save_model(filePath):
# Save current model.
# Args:
# - filePath = path to the cae file to save
    
    mdb.saveAs(pathName=filePath)
    
    return
# End: def save_model

def open_model(filePath):
# Open model.
# Args:
# - filePath = path to the cae file to open
     
    openMdb(pathName=filePath)

    return
# End: def open_model

def get_model_names():

    modelName = mdb.models.keys()[0]
    myModel = mdb.models[modelName]
    partName = myModel.parts.keys()[0]

    return modelName, partName
# End: def get_model_names

def get_displacements(jobName, Nx, Nz, elemType, filePath='', inc=-1):
    # Calculate stress and strain from the reaction forces and displacements of the reference points.
    # Args:
    # - jobName = name of the job
    # - stepName = name of the step
    # - inc = number of the step increment
    # Returns:
    # - stresses = array of stress components
    # - strains = array of strain components

        # Open odb file
        odb = openOdb(jobName + ".odb")

        # Get names
        partName = odb.parts.keys()[0]
        setName = 'Node ' + partName.upper()
        stepName = odb.steps.keys()[0]
        myStep = odb.steps[stepName]
        myHistory = myStep.historyRegions

        # Get nodes
        nodeName = []
        for set in myHistory.keys():
            if setName in set:
                nodeName.append(set)
        
        # Get displacements
        U1 = []
        U3 = []
        for node in nodeName:
            myOutput = myHistory[node].historyOutputs['U1']
            U1.append(myOutput.data[inc][1])
            myOutput = myHistory[node].historyOutputs['U3']
            U3.append(myOutput.data[inc][1])
        U1 = np.mean(U1)
        U3 = np.mean(U3)

        # Store data
        Ntot = Nx * Nx * Nz
        meshSeed = str(Nx) + 'x' + str(Nx) + 'x' + str(Nz)
        data = [jobName, elemType, int(Nx), int(Nz), Ntot, meshSeed, U1, U3]
        if filePath != '':
            if os.path.isfile(filePath):
                with open(filePath, "a") as file:
                    for value in data:  
                        file.write('%s\t' % (value))
                    file.write("\n")
            else:
                header = ['Job', 'Element_Type', 'Num_Horizontal_Elem', 'Num_Vertical_Elem', 'Total_Num_Elem', 'Mesh_Seeds', 'Horizontal_Disp', 'Vertical_Disp']
                with open(filePath, "w") as file:
                    file.write("\t".join(header) + "\n")
                    for value in data:  
                        file.write('%s\t' % (value))
                    file.write("\n")

        odb.close()

        return data
# End: def calculate_stress_strain_from_RP

def get_elements_nodes(jobName, filePath=''):
#
# Args:
# - 

    # Open odb file
    odb = openOdb(jobName + ".odb")
    stepName = odb.steps.keys()[0]
    myStep = odb.steps[stepName]
    myAssembly = odb.rootAssembly
    lastFrame = myStep.frames[-1] # set frame

    myElements = myAssembly.elementSets[' ALL ELEMENTS'].elements # list of all elements
    beamCoord = []
    for i in range(len(myElements)): # for each beam
        myNodes = myAssembly.nodeSets[' ALL NODES'].nodes[i] # list of nodes of the beam
        # Find nodes label
        node_label = []
        for k in range(len(myNodes)):
            node_label.append(myNodes[k].label)
        # Find nodes of each element
        Coord = [] # initialize array of 
        for j in range(len(myElements[i])): # for each element of the beam           
            myConnectivity = myElements[i][j].connectivity # element connectivity
            idx = [node_label.index(myConnectivity[k]) for k in range(len(myConnectivity))] # find nodes index
            Node0 = [] # initialize array of inital coordinates
            U = [] # initialize array of final coordinates
            for k in idx: # for each node of the element
                myRegion = myNodes[k] # set node region
                Node0.append(lastFrame.fieldOutputs['COORD'].getSubset(region=myRegion, position=NODAL).values[0].data) # get initial coord           
                U.append(lastFrame.fieldOutputs['U'].getSubset(region=myRegion, position=NODAL).values[0].data) # get displacement          
            Coord.append([Node0, U])
        beamCoord.append(np.array(Coord))

        # Save data
        if filePath != '':
            np.save(filePath, beamCoord)

        odb.close()

    
    return beamCoord
# End: def get elements_nodes

def sort_mesh(elem_order, elem_integration, Nx, Nz):
#
# Args:
# -
# -
# -
# -

    # Set element type
    if elem_order == 'linear':
        if elem_integration == 'full':
            element = 'C3D8'
        elif elem_integration == 'reduced':
            element = 'C3D8R'
        else:
            raise ValueError('Please set a proper integration type! [full/reduced]')
    elif elem_order == 'quadratic':
        if elem_integration == 'full':
            element = 'C3D20'
        elif elem_integration == 'reduced':
            element = 'C3D20R'
        else:
            raise ValueError('Please set a proper integration type! [full/reduced]')
    else:
        raise ValueError('Please set a proper geometric order! [linear/quadratic]')
    # Set mesh seed
    Ntot = Nx * Nx * Nz
    seed = str(Nx) + 'x' + str(Nx) + 'x' + str(Nz)
    
    return element, seed, Ntot
# End: sort_mesh

def open_odb(jobName):

    # Open odb file
    odb = openOdb(jobName + ".odb")
    session.viewports['Viewport: 1'].setValues(displayedObject=odb)
    session.viewports['Viewport: 1'].odbDisplay.display.setValues(plotState=(CONTOURS_ON_DEF, ))
    session.viewports['Viewport: 1'].odbDisplay.setPrimaryVariable(variableLabel='U', outputPosition=NODAL, refinement=(COMPONENT, 'U1'), )

    return
# End: open_odb

