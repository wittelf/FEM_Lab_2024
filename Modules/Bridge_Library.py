# Alessia Ferrara, February 2024

# Imports
from abaqus import *
from caeModules import *
from abaqusConstants import *
from visualization import *
import glob
import os.path
import numpy as np
import math

def create_bridge_part(nodes, beams):
# Create the bridge model as a network of beams.
# Args:
# - nodes = matrix of nodes data
# - beams = matrix of beams data
# Returns:
# - modelName = name of the model

    Mdb() # open model
    modelName = 'Model-1'
    nb = len(beams)
    partName = 'Beam-'

    for i in range(nb):
        # Beam name
        beamName = partName + str(i)
        # Find nodes
        idx, = np.where(nodes[:,0]==beams[i,1])[0]
        p0 = [nodes[idx][1], nodes[idx][2]]
        idx, = np.where(nodes[:,0]==beams[i,2])[0]
        p1 = [nodes[idx][1], nodes[idx][2]]
        # Set dimensions
        l = math.sqrt(pow(p0[0]-p1[0],2)+pow(p0[1]-p1[1],2))
        h = math.sqrt(beams[i,3])
        b = h
        # Create beam part
        create_beam_part(modelName, beamName, l, b, h)

    return modelName
# End: def create_bridge_part

def create_beam_part(modelName, partName, l, b, h):
# Create beam part.
# Args:
# - modelName = name of the model
# - partName = name of the part
# - l = length of the beam (z)
# - b = thickness of the beam (y)
# - h = width of the beam (x)

    myModel = mdb.models[modelName]

    # Create part          
    mySketch = myModel.ConstrainedSketch(name='__profile__', sheetSize=20.0)
    mySketch.rectangle(point1=(-h/2., 0.0), point2=(h/2., l))
    myModel.Part(name=partName, dimensionality=THREE_D, type=DEFORMABLE_BODY)
    myPart = myModel.parts[partName]
    myPart.BaseSolidExtrude(sketch=mySketch, depth=b)
    del myModel.sketches['__profile__']
    session.viewports['Viewport: 1'].setValues(displayedObject=myPart)   

    return
# End: def create_beam_part

def create_bridge_material(modelName, beams):
# Create and assign a material to each beam of the bridge.
# Args:
# - modelName = name of the model
# - beams = matrix of beams data

    nb = len(beams)
    partName = 'Beam-'

    for i in range(nb):
        # Beam name
        beamName = partName + str(i)
        # Beam properties
        E = beams[i,4] # Young's modulus
        v = beams[i,5] # Poisson ratio
        # create and assign material
        create_isotropic_material(modelName, beamName, E, v, beamName)

    return modelName
# End: def create_bridge_material

def create_isotropic_material(modelName, partName, Emod, Pratio, materialName=''):
# Create and assign an isotropic material.
# Args:
# - Emod = Young's modulus
# - Pratio = Poisson's ratio

    myModel = mdb.models[modelName]
    myPart = myModel.parts[partName]
    if materialName == '':
        materialName = 'Material'

    # Create material
    myModel.Material(name=materialName)
    myMaterial = myModel.materials[materialName]
    myMaterial.Elastic(type=ISOTROPIC, table=((Emod,Pratio), ))
    # Create section
    sectionName = materialName
    myModel.HomogeneousSolidSection(name=sectionName, material=materialName, thickness=None)
    # Assign section
    setName = 'Set-'+partName
    myPart.Set(cells=myPart.cells, name=setName)
    myPart.SectionAssignment(offset=0.0, offsetField='', offsetType=MIDDLE_SURFACE,
        region=myPart.sets[setName], sectionName=sectionName, thicknessAssignment=FROM_SECTION)

    return materialName
# End: def create_isotropic_material

def create_bridge_assembly(modelName, nodes, beams):
# Create bridge assembly.
# Args:
# - modelName = name of the model
# - nodes = matrix of nodes data
# - beams = matrix of beams data
    
    myModel = mdb.models[modelName]
    myAssembly = myModel.rootAssembly
    nb = len(beams)
    partName = 'Beam-'

    for i in range(nb):
        # Beam name
        beamName = partName + str(i)
        # Create assembly
        myPart = myModel.parts[beamName]
        myAssembly.Instance(name=beamName, part=myPart, dependent=ON)
        # Rotate beam
        idx, = np.where(nodes[:,0]==beams[i,1])[0]
        p0 = [nodes[idx][1], nodes[idx][2]]
        idx, = np.where(nodes[:,0]==beams[i,2])[0]
        p1 = [nodes[idx][1], nodes[idx][2]]
        if (p1[0]-p0[0]) == 0:
            alpha = 0.0
        else:
            alpha = -math.degrees(math.pi/2.-math.atan((p1[1]-p0[1])/(p1[0]-p0[0])))
        myAssembly.rotate(instanceList=(beamName, ), axisPoint=(0.0, 0.0, 0.0), axisDirection=(0.0, 0.0, 1.0), angle=alpha)
        # Translate Node 0 from origin to Node 0 coordinates
        myAssembly.translate(instanceList=(beamName, ), vector=(p0[0], p0[1], 0.0))

        # Translate along Z to center the beam
        b = math.sqrt(beams[i,3])
        myAssembly.translate(instanceList=(beamName, ), vector=(0.0, 0.0, -b/2.))

    return
# End: def create_bridge_assembly

def create_static_step(modelName, stepName='', nlgeom=OFF, timePeriod=1., previous="Initial",
                        initialInc=0.1, maxNumInc=100, minInc=1e-5, maxInc=1):
# Create static step.
# Args:
# - modelName = name of the model
# - stepname = name of the step
# - others: optional according to Abaqus nomenclature
    
    myModel = mdb.models[modelName]
    if stepName == '':
        stepName = 'Step-1'
    
    # Create step
    myModel.StaticStep(name=stepName, timePeriod=timePeriod, nlgeom=nlgeom, previous=previous,
                        initialInc=initialInc, maxNumInc=maxNumInc, minInc=minInc, maxInc=maxInc)
    
    return stepName
# End: create_static_step

def create_boundary_conditions(modelName, stepName, nodes, beams):
# Couple nodes to faces and apply boundary conditions to nodes.
# Args:
# Args:
# - modelName = name of the model
# - stepName = name of the step
# - nodes = matrix of nodes data
# - beams = matrix of beams data
    
    myModel = mdb.models[modelName]
    myAssembly = myModel.rootAssembly

    # Create references points corresponding to nodes
    nn = len(nodes)
    for i in range(nn):
        pointName = 'RP-' + str(int(nodes[i][0]))
        RP = myAssembly.ReferencePoint(point=(nodes[i][1], nodes[i][2], 0.0))
        if RP.name != pointName:
            myAssembly.features.changeKey(fromName=RP.name, toName=pointName)
        r = myAssembly.referencePoints
        setName = 'Set-'+ pointName
        myAssembly.Set(referencePoints=(r[RP.id],), name=setName)

    # Create beam face sets
    nb = len(beams)
    partName = 'Beam-'
    for i in range(nb):
        # Beam name
        beamName = partName + str(i)
        myInstance = myAssembly.instances[beamName]
        # Bottom face (Start node)
        idx, = np.where(nodes[:,0]==beams[i,1])[0]
        p0 = [nodes[idx][1], nodes[idx][2]]
        face = myInstance.faces.findAt(((p0[0], p0[1], 0.),),)
        faceName = 'Face-' + beamName + '-0'
        myAssembly.Surface(side1Faces=face, name=faceName)
        # Top face (End node)
        idx, = np.where(nodes[:,0]==beams[i,2])[0]
        p1 = [nodes[idx][1], nodes[idx][2]]
        face = myInstance.faces.findAt(((p1[0], p1[1], 0.),),)
        faceName = 'Face-' + beamName + '-1'
        myAssembly.Surface(side1Faces=face, name=faceName)

    # Couple corresponding face-node
    for i in range(nn):
        for j in range(nb):
            if nodes[i,0]==beams[j,1] or nodes[i,0]==beams[j,2]:
                constName = 'Coupling-' + str(j) + '-' + str(i)
                pointName = 'Set-RP-' + str(int(nodes[i][0]))
                beamName = partName + str(j)
                if nodes[i,0]==beams[j,1]:
                    faceName = 'Face-' + beamName + '-0'
                else:
                    faceName = 'Face-' + beamName + '-1'
                myModel.Coupling(controlPoint=myAssembly.sets[pointName], couplingType=KINEMATIC,
                                 influenceRadius=WHOLE_SURFACE, localCsys=None, name=constName,
                                 surface=myAssembly.surfaces[faceName], u1=ON, u2=ON, u3=ON, ur1=OFF, ur2=OFF, ur3=OFF)

    # Couple corresponding nodes
    for i in range(nn):
        for j in range(i+1, nn):
            if nodes[i,1]==nodes[j,1] and nodes[i,2]==nodes[j,2]:
                constName = 'Node-' + str(j) + '-' + str(i)
                point1 = 'Set-RP-' + str(int(nodes[i][0]))
                point2 = 'Set-RP-' + str(int(nodes[j][0]))
                myModel.Coupling(name=constName, controlPoint=myAssembly.sets[point1], 
                    surface=myAssembly.sets[point2], influenceRadius=WHOLE_SURFACE, couplingType=KINEMATIC, 
                    localCsys=None, u1=ON, u2=ON, u3=ON, ur1=OFF, ur2=OFF, ur3=OFF)

    # Apply boundary conditions
    for i in range(nn):
        pointName = 'Set-RP-' + str(int(nodes[i][0]))
        myRegion = myAssembly.sets[pointName]
        boundName = 'Condition-' + str(i)
        u = []
        for k in range(5,8):
            if nodes[i,k] == 1:
                u.append(0)
            else:
                u.append(UNSET)  
        myModel.DisplacementBC(name=boundName, createStepName=stepName, region=myRegion, u1=u[0], u2=u[1], u3=u[2],
                               ur1=UNSET, ur2=UNSET, ur3=UNSET, amplitude=UNSET, fixed=OFF, distributionType=UNIFORM,
                               fieldName='', localCsys=None)

    return
# End: def create_boundary_conditions

def create_loads(modelName, stepName, nodes, beams):
# Appply loads to nodes.
# Args:
# - modelName = name of the model
# - stepName = name of the step
# - nodes = matrix of nodes data
    
    myModel = mdb.models[modelName]
    myAssembly = myModel.rootAssembly

    # Create aplitude
    ampName = 'Amplitude'
    myModel.EquallySpacedAmplitude(name=ampName, timeSpan=STEP, smooth=SOLVER_DEFAULT,
                                   fixedInterval=1.0, begin=0.0, data=(0.0, 1.0))

    # Apply loads
    nn = len(nodes)
    for i in range(nn):
        pointName = 'Set-RP-' + str(int(nodes[i][0]))
        myRegion = myAssembly.sets[pointName]
        loadName = 'Load-' + str(i)
        if nodes[i,3] != 0 and nodes[i,4] != 0:
            myModel.ConcentratedForce(name=loadName, createStepName=stepName, region=myRegion, cf1=nodes[i,3],
                                      cf2=nodes[i,4], amplitude=ampName, distributionType=UNIFORM, field='', localCsys=None)
        elif nodes[i,3] != 0 and nodes[i,4] == 0:
            myModel.ConcentratedForce(name=loadName, createStepName=stepName, region=myRegion, cf1=nodes[i,3],
                                      amplitude=ampName, distributionType=UNIFORM, field='', localCsys=None)
        elif nodes[i,3] == 0 and nodes[i,4] != 0:
            myModel.ConcentratedForce(name=loadName, createStepName=stepName, region=myRegion, cf2=nodes[i,4],
                                      amplitude=ampName, distributionType=UNIFORM, field='', localCsys=None)
    
    # Create set of top nodes
    h = math.sqrt(beams[5,3])
    idx, = np.where(nodes[:,0]==1)[0]
    p1 = [nodes[idx][1], nodes[idx][2]+h/2.]
    idx, = np.where(nodes[:,0]==10)[0]
    p10 = [nodes[idx][1], nodes[idx][2]+h/2.]
    delta = h/10.
    nb = len(beams)
    partName = 'Beam-'
    for i in range(nb):
        # Beam name
        beamName = partName + str(i)
        # Create set of top nodes
        myInstance = myAssembly.instances[beamName]
        allNodes = myInstance.nodes
        if 'myNodes' in locals():
            myNodes = myNodes + allNodes.getByBoundingBox(p1[0]-delta, p1[1]-delta, h/2.-delta, p10[0]+delta, p10[1]+delta, h/2.+delta)
        else:
            myNodes = allNodes.getByBoundingBox(p1[0]-delta, p1[1]-delta, h/2.-delta, p10[0]+delta, p10[1]+delta, h/2.+delta)

    # Create history output (COOR1, U2)
    setName = 'Top-nodes'
    myRegion = myAssembly.Set(name=setName, nodes=myNodes)
    myModel.HistoryOutputRequest(name='Bridge-HO', createStepName=stepName, variables=('COOR1', 'U2'), 
                                 region=myRegion, numIntervals=10, sectionPoints=DEFAULT, rebar=EXCLUDE)
    # Create field output (MISES, COORD)
    myModel.FieldOutputRequest(name='Bridge-FO', createStepName=stepName, variables=('MISES', 'COORD'), numIntervals=10,
                               region=MODEL, sectionPoints=DEFAULT, position=AVERAGED_AT_NODES, rebar=EXCLUDE)
    
    return
# End: def create_loads

def create_bridge_mesh(modelName, nodes, beams, numelem, elemtype='C3D20R'):
# Create mesh of the bridge.
# Args:
# - modelName = name of the model
# - nodes = matrix of nodes data
# - beams = matrix of beams data
# - numelem = number of elements along the beam
# - geomorder = geometric order (linear or quadratic)
# - integration = integration type (full or reduced)

    myModel = mdb.models[modelName]
    nb = len(beams)
    partName = 'Beam-'

    for i in range(nb):
        # Beam name
        beamName = partName + str(i)
        # Calculate global size
        idx, = np.where(nodes[:,0]==beams[i,1])[0]
        p0 = [nodes[idx][1], nodes[idx][2]]
        idx, = np.where(nodes[:,0]==beams[i,2])[0]
        p1 = [nodes[idx][1], nodes[idx][2]]
        l = math.sqrt(pow(p0[0]-p1[0],2)+pow(p0[1]-p1[1],2))
        global_size = l/numelem
        # Create seeds
        create_global_seeds(modelName, beamName, global_size)
        # Set element type
        set_mesh_element_type(modelName, beamName, elemtype)
        # Generate mesh
        myPart = myModel.parts[beamName]
        myPart.generateMesh()

    myAssembly = myModel.rootAssembly
    myAssembly.regenerate()
    
    return
# End: def create_bridge_mesh

def create_global_seeds(modelName, partName, globalsize, minSizeFactor=0.1, deviationFactor=0.1):
# Create global seeds to mesh.
# Args:
# - modelName = name of the model
# - partName = name of the part
# - globalsize = global size of elements
# - minSizeFactor = fraction of the global element size to set the minimum size
# - deviationFactor = measure of deviation from the original geometry

    myModel = mdb.models[modelName]
    myPart = myModel.parts[partName]

    # Seed part
    myPart.seedPart(deviationFactor=deviationFactor, minSizeFactor=minSizeFactor, size=globalsize)   

    return
# End: def create_global_seeds

def set_mesh_element_type(modelName, partName, elemtype='C3D20'):
# Set element type of the mesh.
# Args: 
# - modelName = name of the model
# - partName = name of the part
# - elemtype = element type (C3D8, C3D8R, C3D20, C3D20R)

    myModel = mdb.models[modelName]
    myPart = myModel.parts[partName]

    if elemtype == 'C3D8':
        # Linear + full integration
        elemType1 = mesh.ElemType(elemCode=C3D8, elemLibrary=STANDARD,
                                    secondOrderAccuracy=OFF, distortionControl=DEFAULT)
        elemType2 = mesh.ElemType(elemCode=C3D6, elemLibrary=STANDARD)
        elemType3 = mesh.ElemType(elemCode=C3D4, elemLibrary=STANDARD)
    elif elemtype == 'C3D8R':
        # Linear + reduced integration
        elemType1 = mesh.ElemType(elemCode=C3D8R, elemLibrary=STANDARD,
                                  kinematicSplit=AVERAGE_STRAIN, secondOrderAccuracy=OFF,
                                  hourglassControl=DEFAULT, distortionControl=DEFAULT)
        elemType2 = mesh.ElemType(elemCode=C3D6, elemLibrary=STANDARD)
        elemType3 = mesh.ElemType(elemCode=C3D4, elemLibrary=STANDARD)
    elif elemtype == 'C3D20':
        # Quadratic + full integration
        elemType1 = mesh.ElemType(elemCode=C3D20, elemLibrary=STANDARD)
        elemType2 = mesh.ElemType(elemCode=C3D15, elemLibrary=STANDARD)
        elemType3 = mesh.ElemType(elemCode=C3D10, elemLibrary=STANDARD)
    elif elemtype == 'C3D20R':
        # Quadratic + reduced integration
        elemType1 = mesh.ElemType(elemCode=C3D20R, elemLibrary=STANDARD)
        elemType2 = mesh.ElemType(elemCode=C3D15, elemLibrary=STANDARD)
        elemType3 = mesh.ElemType(elemCode=C3D10, elemLibrary=STANDARD)
    else:
        raise ValueError('Unknown element type')

    # Set element type
    myPart.setElementType(elemTypes=(elemType1,elemType2,elemType3), regions=(myPart.cells, ))

    return
# End: def change_mesh_element_type

def submit_job(modelName, jobName=''):
# Create and submit job.
# Args:
# - jobName = name of the job
# - modelName = name of the model
    
    # Remove existing .lck file
    fileExt = '.lck'
    filePath = os.getcwd() + '/' + jobName + fileExt
    if os.path.exists(filePath):
        os.remove(filePath)

    # Create job
    if jobName == '':
        jobName = 'Job-1'
    mdb.Job(name=jobName, model = modelName, type=ANALYSIS, atTime=None, contactPrint=OFF, description='', echoPrint=OFF,
        explicitPrecision=SINGLE, getMemoryFromAnalysis=True, historyPrint=OFF, memory=90, memoryUnits=PERCENTAGE, 
        modelPrint=OFF, multiprocessingMode=DEFAULT, nodalOutputPrecision=SINGLE, numCpus=1, queue=None, scratch='', 
        userSubroutine='', waitHours=0, waitMinutes=0)
    
    # Run job
    myJob = mdb.jobs[jobName]
    myJob.submit(consistencyChecking=OFF)
    myJob.waitForCompletion()

    # Remove simulation files except odb
    path_all = glob.glob(os.getcwd() + '/' + jobName + '.*')
    extension = ('.odb','.simdir','.log') #, '.res', '.prt', '.mdl', '.stt') # extension to keep
    path_remove = [filename for filename in path_all if not filename.endswith(extension)]
    [os.remove(filePath) for filePath in path_remove]

    """# Remove abaqus.rpy files
    path_remove = glob.glob(os.path.dirname(os.getcwd()) + '/' + 'abaqus.rpy*')
    [os.remove(filePath) for filePath in path_remove]"""
        
    return
# End: def submit_job

def save_model(filePath):
# Save current model.
# Args:
# - filePath = path to the cae file to save
    
    mdb.saveAs(pathName=filePath)
    
    return
# End: def save_model

def open_model(filePath):
# Open model.
# Args:
# - filePath = path to the cae file to open
     
    openMdb(pathName=filePath)

    return
# End: def open_model

def get_model_names():

    modelName = mdb.models.keys()[0]
    myModel = mdb.models[modelName]
    partName = myModel.parts.keys()[0]

    return modelName, partName
# End: def get_model_names

def get_results(jobName, nodes, elem_type, num_elem, filePath=''):
# Calculate stress and strain from the reaction forces and displacements of the reference points.
# Args:
# - jobName = name of the job
# - 

    # Open odb file
    odb = openOdb(jobName + ".odb")
    stepName = odb.steps.keys()[0]
    myStep = odb.steps[stepName]
    myAssembly = odb.rootAssembly
        
    # Get data from hiytory output (COOR1, U2)
    # Get nodes list
    myHistory = myStep.historyRegions
    nodeName = []
    for set in myHistory.keys():
        if set!='Assembly ASSEMBLY':
            nodeName.append(set)
    # Store data
    X1 = []
    U2 = []
    for node in nodeName:
        myOutput = myHistory[node].historyOutputs['COOR1']
        X1.append(myOutput.data[0][1])
        myOutput = myHistory[node].historyOutputs['U2']
        U2.append(myOutput.data[-1][1])
    idx = np.argsort(X1)
    X1 = [X1[i] for i in idx]
    U2 = [U2[i] for i in idx]

    # Get data from field output (MISES)
    lastFrame = myStep.frames[-1]
    myElements = myAssembly.elementSets[' ALL ELEMENTS'].elements # list of all elements
    maxVM = []
    posVM = []
    for i in range(len(myElements)): # for each beam
        myNodes = myAssembly.nodeSets[' ALL NODES'].nodes[i+1] # list of nodes of the beam
        # Find nodes label
        node_label = []
        for k in range(len(myNodes)):
            node_label.append(myNodes[k].label)
        VM = []
        Centroid = []
        for j in range(len(myElements[i])): # for each element of the beam
            myStress = lastFrame.fieldOutputs['S'].getSubset(region=myElements[i][j], position=CENTROID).getScalarField(invariant=MISES,)
            VM.append(myStress.values[0].data)
            myConnectivity = myElements[i][j].connectivity # element connectivity
            idx = [node_label.index(myConnectivity[k]) for k in range(len(myConnectivity))] # find nodes index
            Node0 = [] # initialize array of inital coordinates
            for k in idx: # for each node of the element
                myRegion = myNodes[k] # set node region
                Node0.append(lastFrame.fieldOutputs['COORD'].getSubset(region=myRegion, position=NODAL).values[0].data)
            Node0 = np.array(Node0)
            Centroid.append([(np.max(Node0[:,0])+np.min(Node0[:,0]))/2., (np.max(Node0[:,1])+np.min(Node0[:,1]))/2., (np.max(Node0[:,2])+np.min(Node0[:,2]))/2.])

        maxVM.append(np.max(VM)) # max of the beam
        idx = VM.index(np.max(VM)) # position of max
        posVM.append(Centroid[idx]) # corresponding centroid

    posVM = posVM[maxVM.index(np.max(maxVM))]
    maxVM = np.max(maxVM)
    
    # Store data
    idx = np.nonzero(nodes[:,4])[0]
    data = [jobName, elem_type, num_elem, nodes[idx,1][0], nodes[idx,4][0], X1, U2, maxVM, posVM]
    # Save data
    if filePath != '':
        if os.path.isfile(filePath):
            with open(filePath, "a") as file:
                for value in data:  
                    file.write('%s\t' % (value))
                file.write("\n")
        else:
            header = ['Job', 'Element_type', 'Num_elements', 'Load_position', 'Load_value', 'Nodes_position', 'Vertical_displacements', 'Max_VonMises', 'Max_VonMises_position']
            with open(filePath, "w") as file:
                file.write("\t".join(header) + "\n")
                for value in data:  
                    file.write('%s\t' % (value))
                file.write("\n")

    odb.close()

    return
# End: def get_results

def get_elements_nodes(jobName, filePath):
#
# Args:
# - 

    # Open odb file
    odb = openOdb(jobName + ".odb")
    stepName = odb.steps.keys()[0]
    myStep = odb.steps[stepName]
    myAssembly = odb.rootAssembly
    lastFrame = myStep.frames[-1] # set frame

    myElements = myAssembly.elementSets[' ALL ELEMENTS'].elements # list of all elements
    beamCoord = []
    for i in range(len(myElements)): # for each beam
        myNodes = myAssembly.nodeSets[' ALL NODES'].nodes[i+1] # list of nodes of the beam
        # Find nodes label
        node_label = []
        for k in range(len(myNodes)):
            node_label.append(myNodes[k].label)
        # Find nodes of each element
        Coord = [] # initialize array of 
        for j in range(len(myElements[i])): # for each element of the beam           
            myConnectivity = myElements[i][j].connectivity # element connectivity
            idx = [node_label.index(myConnectivity[k]) for k in range(len(myConnectivity))] # find nodes index
            Node0 = [] # initialize array of inital coordinates
            U = [] # initialize array of final coordinates
            for k in idx: # for each node of the element
                myRegion = myNodes[k] # set node region
                Node0.append(lastFrame.fieldOutputs['COORD'].getSubset(region=myRegion, position=NODAL).values[0].data) # get initial coord           
                U.append(lastFrame.fieldOutputs['U'].getSubset(region=myRegion, position=NODAL).values[0].data) # get displacement          
            Coord.append([Node0, U])
        beamCoord.append(np.array(Coord))

    # Save data
    np.save(filePath, beamCoord, allow_pickle=True, fix_imports=True)

    odb.close()

    return beamCoord
# End: def get elements_nodes

def sort_mesh(elem_order, elem_integration):
#
# Args:
# -
# -

    # Set element type
    if elem_order == 'linear':
        if elem_integration == 'full':
            element = 'C3D8'
        elif elem_integration == 'reduced':
            element = 'C3D8R'
        else:
            raise ValueError('Please set a proper integration type! [full/reduced]')
    elif elem_order == 'quadratic':
        if elem_integration == 'full':
            element = 'C3D20'
        elif elem_integration == 'reduced':
            element = 'C3D20R'
        else:
            raise ValueError('Please set a proper integration type! [full/reduced]')
    else:
        raise ValueError('Please set a proper geometric order! [linear/quadratic]')
    
    return element
# End: sort_mesh
