
# Imports
import os
import imp
import sys

# Set index to read inputs
execMode = sys.argv[-1]
if execMode == 'noGUI':
    idx = 10
elif execMode == 'script':
    idx = 8
else:
    raise ValueError('Please, set the correct execution mode: script or noGUI.')

# Set working directory
libPath = sys.argv[idx]
print(libPath)
setPath =  r'{}'.format(libPath)
os.chdir(setPath)

# Import beam library
import Beam_Library as beam
imp.reload(beam)
from Beam_Library import *

# Set input variables
# Cae path
caePath = sys.argv[idx+1] # cae file path
# Geometry variables
l = float(sys.argv[idx+2]) # z [m]
b = float(sys.argv[idx+3]) # y [m]
h = float(sys.argv[idx+4]) # x [m]
# Material parameters
Emod = float(sys.argv[idx+5]) # Young's modulus [Pa]
Pratio = float(sys.argv[idx+6]) # Poisson's ratio [-]
# Loads
Fx = float(sys.argv[idx+7]) # horizontal force [N] -> bending
Fz = float(sys.argv[idx+8]) # vertical force [N] -> compression
# Mesh parameters
numElem_vert = int(sys.argv[idx+9]) # number of elements along the beam
numElem_hor = int(sys.argv[idx+10]) # number of elements along the beam
elemType = sys.argv[idx+11] # element type
jobName = sys.argv[idx+12] # job name
filePath = sys.argv[idx+13] # save file path

# Create beam model
# Create part
modelName, partName = beam.create_beam_part(l, b, h)
# Create and assign material
materialName = beam.create_isotropic_material(modelName, partName, Emod, Pratio)
# Create assembly
beam.create_beam_assembly(modelName, partName)
# Create static step
stepName = beam.create_static_step(modelName)
# Create boundary conditions
beam.create_boundary_conditions(modelName, stepName, partName, l, b, h)
# Create loads
beam.create_loads(modelName, stepName, partName, b, h, Fx, Fz)
# Create mesh
beam.create_local_seeds(modelName, partName, l, b, h, numElem_vert, numElem_hor)
beam.create_mesh(modelName, partName, b, h, numElem_hor) # generate mesh
beam.set_mesh_element_type(modelName, partName, elemType) # set element type
# Save model
beam.save_model(caePath)
#print >> sys.__stdout__, "The model has been saved to " + caePath + " ."
# Create and submit job
beam.submit_job(modelName, jobName)
#print >> sys.__stdout__, "Job " + jobName + " completed."
# Read results
data = beam.get_displacements(jobName, numElem_hor, numElem_vert, elemType, filePath)
print >> sys.__stdout__, data
if execMode == 'script':
    beam.open_odb(jobName)

    


