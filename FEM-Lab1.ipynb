{
 "cells": [
  {
   "cell_type": "markdown",
   "id": "4f0afad9-ddbb-48ab-9293-1e655c89ac65",
   "metadata": {},
   "source": [
    "---\n",
    "title: Materials for Construction Engineering - FEM Laboratory - FS 2024 \n",
    "subtitle: A brief introduction to FEM with Abaqus - Part 1 - First steps\n",
    "authors:\n",
    "  - name: Falk Wittel (fwittel@ethz.ch)\n",
    "  - name: Alessia Ferrara (aferrara@ethz.ch)\n",
    "  - name: Julio O. Amando de Barros (jortiz@student.ethz.ch)\n",
    "---"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "faf67b19-18d0-40fe-a457-166bcbb22a09",
   "metadata": {},
   "source": [
    "This notebook is part of the 2023 revised Materials for Civil Engineering course. It is the first of two notebooks that build upon each other. Version: 0.0.2024"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "3b5f93a9-7abd-4ef0-97db-cdab0bfb1498",
   "metadata": {
    "tags": []
   },
   "source": [
    ":::{important} Learning Goals &#9971;\n",
    "- [ ] I prepared appropriately for this lab (approx. 45min)\n",
    "- [ ] I can reproduce the workflow for modeling simple parts in Abaqus.\n",
    "- [ ] I know which elements to take for my calculations and where to be cautious.\n",
    "- [ ] I understand why I should always make calculations at different discretization levels.  \n",
    ":::"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "1c39b7f8-8285-456f-b635-30f32a0c980d",
   "metadata": {},
   "source": [
    "Prerequisites:\n",
    ": You learned about the fundaments of FEM and proved it in the entrance test. You gained an idea through the sequence of movies of how to work with a FEM program and how we interact via Python. Basic Python knowledge from the Digital Engineering course is needed."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "85a730a1-b371-4fe0-9b13-79fbfca40f54",
   "metadata": {
    "tags": []
   },
   "source": [
    ":::{hint}Instructions for working in the FEM-Lab &#9756;\n",
    ":class: dropdown\n",
    "Opposite to the Digital Engineering Notebooks, this Notebook explores the possibilities of MySt. For you as user, there are almost no changes, only improved interactivity. \n",
    "1. Work through the notebook, following the prescribed tasks, answer all questions and provide all plots and interpretation asked.\n",
    "2. Already take notes in the respective cells during the lab that you can elaborate after class.\n",
    "3. When you are done, make a final check of all your inputs and the learning goals, generate a pdf, and upload it on moodle in the assignment section of the lab. \n",
    ":::"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "729d3295-ce5d-4549-892a-77140a6fc27d",
   "metadata": {
    "tags": []
   },
   "source": [
    "____\n",
    "\n",
    "## Table of Contents\n",
    "#### [](#h-basic-structure)\n",
    "\n",
    "#### [](#h-modsim)\n",
    "\n",
    "#### [](#h-study1)\n",
    "\n",
    "#### [](#h-summary)\n",
    "\n"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "1b181585-88b8-4096-b9e3-f9484146d8af",
   "metadata": {
    "tags": []
   },
   "source": [
    "_____\n",
    "\n",
    "(h-basic-structure)=\n",
    "# 1. Getting started\n",
    "&#9989; Task 1: Before starting you need to ...\n",
    "\n",
    "1. execute the import cell below.\n",
    "2. fill in your ETH matriculation number and Name in the consecutive cell and **execute it**. These are used to generate your individual tasks. "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "89f0d85c-4d0a-4f01-84a6-6ba00b4e81f9",
   "metadata": {
    "tags": []
   },
   "outputs": [],
   "source": [
    "# Import libraries\n",
    "from pathlib import Path\n",
    "import numpy as np\n",
    "import pandas as pd\n",
    "import time\n",
    "from tqdm.notebook import tqdm      # for waitbar\n",
    "import ipywidgets as widgets\n",
    "from ipywidgets import interactive\n",
    "from mpl_toolkits.mplot3d import Axes3D\n",
    "import matplotlib.pyplot as plt\n",
    "from subprocess import PIPE, run\n",
    "import ast\n",
    "import sys; sys.path.append('../')\n",
    "import Modules.Supplementary_Library as suplib   \n",
    "cwd = Path.cwd() # path to current directory # Set working folder"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "224cda44-45c5-451e-8a5c-eeece91f4d9f",
   "metadata": {
    "tags": []
   },
   "outputs": [],
   "source": [
    "# Student data ###################\n",
    "Name = 'Falk '\n",
    "Surname = 'Wittel'\n",
    "MatriculationNumber = 2090355\n",
    "##################################\n",
    "np.random.seed(MatriculationNumber) # Set random generator seed\n",
    "myrand = np.random.rand(10)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "43a09636-c943-409a-9c37-5adf9b8f7910",
   "metadata": {},
   "source": [
    "```{figure} img/FEM1_P1.png\n",
    ":width:50%\n",
    ":name: Model idea\n",
    "(a) Cable-stayed bridge Ting Kau, Hong Kong (1999, Schlaich, Bergermann & Partner) and (b) strong abstractions of the center pole with notation of geometry and acting forces.\n",
    "```"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "6326e3d3-9e68-49e0-a650-799861c9d6a3",
   "metadata": {},
   "source": [
    ":::{tip} _In this notebook,_ \n",
    "we focus on the importance of the choice of element types, order of integration, discretization, and shape. We model a simple cantilever beam of length $l$, width $b$, and height $h$ made of a material with Young's modulus $E$ under longitudinal ($F_z$) and transverse ($F_x$) load and compare the FEM solution we obtain with the analytical prediction for:\n",
    "> - deflection from bending: $u_{x} = \\frac{4F_x l^3}{Ebh^3} $ and\n",
    "> - longitudinal compression: $u_{z} = \\frac{lF_z}{Ebh}$\n",
    ":::"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "d732837f-afe1-4321-853a-74286464dbad",
   "metadata": {},
   "source": [
    "&#9989; Task 2: Run the cell below to obtain your individual settings.\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "f21c00aa-3be5-430d-97ee-068e8cd8544b",
   "metadata": {
    "tags": []
   },
   "outputs": [],
   "source": [
    "# Set files paths\n",
    "modulespath = cwd / 'Modules'\n",
    "resultspath = cwd / 'Beam_Results.csv'                # path to the results file\n",
    "# Check if results file already exists, if so delete it\n",
    "if resultspath.exists():    resultspath.unlink()\n",
    "\n",
    "# Set input variables -> Hide random generation of variables with external function?\n",
    "inputdata =\t{\n",
    "  'l [m]': np.round(80+70*myrand[0]),  # length of the beam [m]\n",
    "  'b [m]': np.round(2+3*myrand[1]),    # thickness of the beam [m]\n",
    "  'h [m]': np.round(5+5*myrand[2]),    # width of the beam [m]\n",
    "  'E-mod [Pa]': 35e9+10e9*myrand[3],   # Young's modulus [Pa]\n",
    "  'P-ratio [-]': 0.3,                  # Poisson's ratio [-]\n",
    "  'Fx [N]': 10e3+5e3*myrand[4],        # horizontal force [N]\n",
    "  'Fz [N]': 30e6+70e6*myrand[5],       # vertical force [N]\n",
    "  'num_elem': 6,                       # number of elements\n",
    "  'geom_order': 'linear',              # geometric order ('linear' or 'quadratic')\n",
    "  'integration_type': 'full'  }        # integration type ('full' or 'reduced')\n",
    "\n",
    "# Create dataframe\n",
    "df = pd.DataFrame(inputdata, index=[0])\n",
    "print(\"Beam properties:\")\n",
    "formats = ['{:,.0f}', '{:,.0f}', '{:,.0f}', '{:,.2e}', '{:,.1f}', '{:,.2e}', '{:,.2e}', '{:,.0f}']\n",
    "suplib.nice_dataframe_display(df, formats)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "2f7a2033-bbef-4094-8420-11bdca751bd1",
   "metadata": {},
   "source": [
    "&#9989; Task 3: Run the cell below to obtain the theoretical deflection and compression.\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "a90f3770-2207-453e-ba48-91e88ebf9389",
   "metadata": {
    "tags": []
   },
   "outputs": [],
   "source": [
    "# Calculate displacements from Bernoulli's Theory\n",
    "ux_t = 4*inputdata['Fx [N]']*pow(inputdata['l [m]'],3)/(inputdata['E-mod [Pa]']*inputdata['b [m]']*pow(inputdata['h [m]'],3)) # horizontal displacement\n",
    "uz_t = inputdata['Fz [N]']*inputdata['l [m]']/(inputdata['E-mod [Pa]']*inputdata['b [m]']*inputdata['h [m]']) # vertical displacement\n",
    "\n",
    "print(f\"Theoretical displacements: \\n ux_t = {ux_t:,.6f} \\n uz_t = {uz_t:,.6f}\")"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "6aa6fe40-4e73-4c66-a7ff-9490f4134310",
   "metadata": {
    "tags": []
   },
   "source": [
    "_____\n",
    "\n",
    "(h-modsim)=\n",
    "# 2. Modeling and Simulation in Abaqus CAE\n",
    "To get started, you will generate a volume model of the pole in Abaqus CAE with linear elastic material, encastred at the bottom and loaded at all top vertices using linear, fully integrated brick elements (called C3D8 in Abaqus)."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "7b82ef5f-e743-489b-a155-d2bbb412893e",
   "metadata": {},
   "source": [
    ":::{seealso} I have no clue how to work with Abaqus CAE!&#9756;\n",
    ":class: dropdown\n",
    "<video width=\"320\" height=\"240\" \n",
    "       src=\"/imgsabaqusCAEl.mp4\"  \n",
    "       controls>\n",
    "</video>\n",
    ":::"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "6c0ca015-f5d3-4319-9b36-35083b3f627f",
   "metadata": {},
   "source": [
    ":::{tip} _Exercise 1:_ Perform together with the TAs the following tasks:\n",
    "- _Generate the model geometry in the **Part** module_\n",
    "- _Declare a linear elastic isotropic material, a solid section with this material. Assign the section to the pole in the **Properties** modulue._\n",
    "- _Load an instance of your pole into the simulation space in the **Assembly** module._\n",
    "- _Define a Static, General step in the **Step** module._\n",
    "- _Define the encastered bottom and the loads at the top in the **Load** module._\n",
    "- _Mesh the pole with 6 linear, fully integrated 8-node elements in the **Mesh** module_\n",
    "- _Define a job and run it in the **Job** module._\n",
    "- _Open the result odb and measure deflection and compression of the top surface in the **Visualization** module_\n",
    ":::"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "a634d4e2-a870-45f4-8c19-89cb03b2f8e7",
   "metadata": {},
   "source": [
    "&#9989; Task 4: Describe how you modeled the forces and briefly discuss the pros and cons. What alternative would be more \"realistic\"?"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "a69da11d-2066-4910-8182-e829a4c48090",
   "metadata": {},
   "source": [
    ":::{caution} **Explain:** \n",
    "( _Insert your text here after class!_ )\n",
    "- how it was done\n",
    "- pros\n",
    "- cons\n",
    "- alternative\n",
    ":::"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "11e656ef-860b-48f9-878f-f4563e8c1030",
   "metadata": {},
   "source": [
    "&#9989; Task 5: Compare your result to the analytical solution. You can do this by hand or run the cell below with the Job name you gave to the calculation."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "3964fb47-3a66-4fb7-b02a-b1fb92773926",
   "metadata": {
    "tags": []
   },
   "outputs": [],
   "source": [
    "# Set paths and file names\n",
    "filepath2 = modulespath / 'Read_Beam_Results.py' # path to the script to read odb file (without saving)\n",
    "job_name = 'Job-1' # name of the job\n",
    "jobpath = modulespath / (job_name+'.odb')\n",
    "\n",
    "if jobpath.exists(): # only perform evaluation if the odb exists!\n",
    "    # Set execution mode\n",
    "    exec_mode = 'noGUI' # 'script' to open GUI, 'noGUI' to run without GUI\n",
    "    # Create string of inputs\n",
    "    keys=list(inputdata.keys())\n",
    "    elem_type = suplib.sort_mesh(inputdata['geom_order'], inputdata['integration_type']) # set element type\n",
    "    readresults_inputs = \" -- \" + str(\" \".join([str(modulespath), job_name, str(1), str(inputdata[keys[-3]]), elem_type, exec_mode]))\n",
    "    # Run script\n",
    "    output = run(\"abaqus cae \" + exec_mode + \"=\" + str(filepath2) + readresults_inputs, stdout=PIPE, stderr=PIPE, universal_newlines=True, shell=True)\n",
    "    output = ast.literal_eval(output.stdout.splitlines()[-1]) # convert string representation into list\n",
    "    suplib.display_beam_results(output, ux_t, uz_t) # display results\n",
    "else:    print(\"There is no \" + job_name + \" in \" + str(modulespath))\n",
    "    "
   ]
  },
  {
   "cell_type": "markdown",
   "id": "0c746fc5-e036-4f1a-8e45-25910ce9adf8",
   "metadata": {},
   "source": [
    ":::{caution} **Explain:** \n",
    "( _Insert your text here after class!_ )\n",
    "- The results are ...\n",
    "- Why is the structure behaving too stiff?\n",
    ":::"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "9125280a-0e24-43c2-bf70-2d2b268adee3",
   "metadata": {},
   "source": [
    "_____\n",
    "\n",
    "(h-study1)=\n",
    "# 3. Studying discretization and elements\n",
    "Most likely you are disappointed by the very limited accuracy of your first FEM simulation. In class, you heard that with just enough elements the method converges to the exact solution. Also, you heard that different element types exhibit different behavior. In the next exercise, we want to explore this. Since these are very repetitive tasks, we made a collection of python functions, that do the job for you. It works the following:"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "a083fae7-765b-4f68-9591-d87fce5fa352",
   "metadata": {},
   "source": [
    ":::{tip} _Exercise 2:_\n",
    "1. You pick in the widget the different element types you want to test and the range of horizontal and vertical elements.\n",
    "2. When you press <code>calculate</code> you start the bundle of simulations and the results are written in the pandas dataframe displayed.\n",
    "3. You explore this data with the tasks under the different aspects described in the task.\n",
    ":::"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "f9de3da0-aedc-48a9-a6d2-d461787d33ab",
   "metadata": {
    "jupyter": {
     "source_hidden": true
    },
    "tags": []
   },
   "outputs": [],
   "source": [
    "#hidden cell for the widget \n",
    "def select_props(vert, vertsteps, hor, horsteps, Eltypes):\n",
    "    global ElemTypes, num_elem_vert_array, num_elem_hor_array\n",
    "    ElemTypes = Eltypes\n",
    "    num_elem_vert_array = np.arange(vert[0], vert[1], vertsteps, dtype=int)\n",
    "    num_elem_hor_array = np.arange(hor[0], hor[1], horsteps, dtype=int)\n",
    "    \n",
    "    x = np.linspace(0,inputdata['b [m]'],num_elem_hor_array[-1]+1)\n",
    "    y = np.linspace(0,inputdata['h [m]'],num_elem_hor_array[-1]+1)\n",
    "    z = np.linspace(0,inputdata['l [m]'],num_elem_vert_array[-1]+1)\n",
    "    X, Y, Z = np.meshgrid(x,y,z, indexing='ij')\n",
    "\n",
    "    # Create a new figure for plotting\n",
    "    fig = plt.figure()\n",
    "    ax = fig.add_subplot(111, projection='3d')\n",
    "\n",
    "    # Plot the points - you can use a scatter plot for individual points\n",
    "    ax.scatter(X.flatten(), Y.flatten(), Z.flatten(), alpha=0.6)\n",
    "\n",
    "    # Optionally, plot the lines connecting the points to visualize the grid structure\n",
    "    # This can be resource-intensive for large grids, so it's commented out by default\n",
    "    for i in range(num_elem_hor_array[-1]+1):\n",
    "         for j in range(num_elem_hor_array[-1]+1):\n",
    "                 for k in range(num_elem_vert_array[-1]):\n",
    "                         ax.plot([X[i, j, k], X[i, j, k + 1]], [Y[i, j, k], Y[i, j, k + 1]], [Z[i, j, k], Z[i, j, k + 1]], color='k')\n",
    "                 for k in range(num_elem_vert_array[-1]+1):\n",
    "                     if i < num_elem_hor_array[-1]:  ax.plot([X[i, j, k], X[i + 1, j, k]], [Y[i, j, k], Y[i + 1, j, k]], [Z[i, j, k], Z[i + 1, j, k]], color='k')\n",
    "                     if j < num_elem_hor_array[-1]:  ax.plot([X[i, j, k], X[i, j + 1, k]], [Y[i, j, k], Y[i, j + 1, k]], [Z[i, j, k], Z[i, j + 1, k]], color='k')\n",
    "\n",
    "    ax.set_xlabel('X axis'),     ax.set_ylabel('Y axis'),   ax.set_zlabel('Z axis'), ax.set_title('Largest Model (out of scale)')\n",
    "    plt.show()\n",
    "    CRED, CEND = '\\033[91m',  '\\033[0m'\n",
    "    print(CRED + 'The total number of simulations will be {}!\\nIf you want to avoid time issues, run at most 4 simulations.'.format(len(num_elem_vert_array)*len(num_elem_hor_array)*len(ElemTypes)) + CEND)\n",
    "    return \n",
    "\n",
    "w1 = interactive(select_props, vert=widgets.IntRangeSlider(value=[6, 10], min=2, max=20, step=2, description='Vert elem:', continuous_update=False),\n",
    "                 vertsteps = widgets.IntSlider(value=2, min=1, max=10, step=1, description='Vert steps:', continuous_update=False),\n",
    "                 hor=widgets.IntRangeSlider(value=[1, 3], min=1, max=10, step=1, description='Hor elem:', continuous_update=False),\n",
    "                 horsteps = widgets.IntSlider(value=2, min=1, max=10, step=1, description='Hor steps:', continuous_update=False),\n",
    "                 Eltypes = widgets.SelectMultiple( options=['C3D8','C3D8R','C3D20','C3D20R'], value=['C3D8'], description='Eltypes:')\n",
    "                )\n",
    "w = w1.children\n",
    "# CSS-Layouts von aussen nach innen\n",
    "layout_container_aussen = widgets.Layout(display='flex', flex_flow='row', justify_content='space-around', max_width='80%', border='solid 2px', padding='0px 10px')\n",
    "layout_container_eingabe = widgets.Layout(display='flex', flex_flow='column', align_items='stretch', ustify_content='space-between', margin='10px 0px')\n",
    "layout_container_ausgabe = widgets.Layout(border='solid 1px gray',  margin='10px 0px')\n",
    "\n",
    "# Innere Boxen erstellen\n",
    "box_parameter = widgets.VBox(w[:-1], layout=layout_container_eingabe)\n",
    "box_ausgabe   = widgets.VBox([w[-1]], layout = layout_container_ausgabe)\n",
    "\n",
    "# Auessere Box erstellen\n",
    "box_alles = widgets.HBox([box_parameter,box_ausgabe] , layout=layout_container_aussen)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "a7dce5a2-f218-4e6b-9fb0-2d6c676fda41",
   "metadata": {
    "tags": []
   },
   "outputs": [],
   "source": [
    "display(box_alles)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "dc29ac9f-94d6-4ded-aa7e-0407e9429307",
   "metadata": {
    "tags": []
   },
   "outputs": [],
   "source": [
    "caepath = modulespath / 'Beam_Model.cae'\n",
    "job_name = 'Job-Beam' # name of the job\n",
    "\n",
    "pbar = tqdm(total=len(num_elem_vert_array)*len(num_elem_hor_array)*len(ElemTypes))     #Timebar initiiated\n",
    "i = 1  \n",
    "keys=list(inputdata.keys())\n",
    "\n",
    "for num_elem_vert in num_elem_vert_array:# Run simulations\n",
    "    for num_elem_hor in num_elem_hor_array:\n",
    "        for elem_type in ElemTypes:\n",
    "            pbar.update(i-pbar.n)   # Zeitbalken updaten\n",
    "            i += 1\n",
    "            # Set execution mode\n",
    "            exec_mode = 'noGUI' # 'script' to open GUI, 'noGUI' to run without GUI\n",
    "            # Create string of inputs\n",
    "            if i == 2:\n",
    "                createmodel_inputs = \" -- \" + str(\" \".join([str(modulespath),  str(caepath)] + [str(inputdata[mykeys]) for mykeys in keys[:-3]] + [str(num_elem_vert), str(num_elem_hor), elem_type, job_name, str(resultspath), exec_mode]))\n",
    "                filepath3 = modulespath / 'Create_Beam_Model.py' # path to the script to create the beam model\n",
    "            else:\n",
    "                createmodel_inputs = \" -- \" + str(\" \".join([str(modulespath), str(caepath)] + [str(inputdata[mykeys]) for mykeys in keys[:3]] + [str(num_elem_vert), str(num_elem_hor), elem_type, job_name, str(resultspath), exec_mode]))\n",
    "                filepath3 = modulespath /  'Modify_Beam_Model.py' # path to the script to modify the model mesh\n",
    "            # Run script\n",
    "            run(\"abaqus cae \" + exec_mode + \"=\" + str(filepath3) + createmodel_inputs, stdout=PIPE, stderr=PIPE, universal_newlines=True, shell=True)\n",
    " \n",
    "print('Jobs run completed.')\n",
    "pbar.close()            #Zeitbalken schliessen        \n"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "eb5bbcff-70cf-46f3-bd74-d9a39d5254e5",
   "metadata": {},
   "source": [
    "&#9989; Task 6: Explore the effect of the number of horizontal and vertical elements on the accuracy for predicting the deflection and compression:\n",
    "- generate a plot where you have on the abscissa the number of vertical elements and on the ordinate(s) the relative error. Do this for all element types. "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "babadfa7-8890-4786-8ee0-b758ba012e63",
   "metadata": {
    "tags": []
   },
   "outputs": [],
   "source": [
    "#help with extraction and plotting\n",
    "\n",
    "df = pd.read_csv(resultspath, sep='\\t', index_col=False)\n",
    "#extend with the relative errors\n",
    "df['Rel_Horizontal_Disp'] = df['Horizontal_Disp'] / ux_t  # Replace * with your operation\n",
    "df['Rel_Vertical_Disp'] = df['Vertical_Disp'] / uz_t  # Replace * with your operation\n",
    "display(df)\n",
    "\n",
    "ElemTypes = df['Element_Type'].unique() # list of element types in the dataframe\n",
    "\n",
    "fig, ax = plt.subplots(figsize=(6,4))\n",
    "for i in range(len(ElemTypes)):\n",
    "    df2 = df.loc[df['Element_Type'] == ElemTypes[i]].query(\"Num_Horizontal_Elem == 1\")\n",
    "    ax.plot(df2['Num_Vertical_Elem'], df2['Rel_Horizontal_Disp'],'o-',label='deflection '+ElemTypes[i])\n",
    "    ax.plot(df2['Num_Vertical_Elem'], df2['Rel_Vertical_Disp'],'*',label='compression '+ElemTypes[i])\n",
    "\n",
    "ax.set_xlabel('Num_Vertical_Elem')  # Optional: Set the x-axis label\n",
    "ax.set_ylabel('Rel. Disp.')  # Optional: Set the y-axis label\n",
    "#ax.set_ylim([0.9,1.3])\n",
    "ax.legend()\n",
    "ax.set_title('Convergence plots')  # Optional: Set the title\n",
    "plt.show()"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "055c0031-c048-4be0-a858-5d8e304bfe2c",
   "metadata": {},
   "source": [
    ":::{caution} **Explain:** \n",
    "( _Insert your text here after class covering the following topics!_ )\n",
    "- Discuss convergence for the different element types.\n",
    "- What is the effect of the number of elements in the horizontal direction and why?\n",
    "- Why are the linear reduced integration elements problematic?\n",
    "- What are the best elements and why?\n",
    ":::"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "d8d9a66d-3e30-4d4a-9d4a-91c7ceb90483",
   "metadata": {
    "tags": []
   },
   "source": [
    ":::::{seealso} Hint\n",
    ":class: dropdown\n",
    "Check the lecture slides about elements and their performance if you are uncertain. \n",
    ":::::"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "9eb95a62-1913-47b5-a253-bb6cb260e827",
   "metadata": {
    "tags": []
   },
   "source": [
    "_____\n",
    "\n",
    "(h-summary)=\n",
    "# 4. Summary\n",
    "As you realized, FEM simulations are not bullet-proof and require expert knowledge and experience from your side. It is easy to calculate nonsense, but not easy to realize that you did so. You learned already some techniques to gain certainty:\n",
    "\n",
    "1. Always calculate with different elements you understand and use different meshes to see if you have discretization effects.\n",
    "2. If you are uncertain, try to calculate simple situations first, where you know the correct solution.\n",
    "3. Don't ignore warnings from the software and read the manual.\n",
    "4. Don't sacrifice accuracy for speed, but also be aware of time limitations. _Start simple and then improve_.\n",
    "5. Try out different model approaches e.g. for boundary conditions to be able to judge the effect of simplifications.\n",
    "6. Don't forget the theoretical background you learned, since _practice without theory is dangerous_."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "a810672b-3088-407e-981a-b8f1fa5759f4",
   "metadata": {
    "tags": []
   },
   "outputs": [],
   "source": [
    "#hidden cell for the widget \n",
    "def select_props1(vert,  hor,  Eltypes, myruntype):\n",
    "    global elem_type, num_elem_vert, num_elem_hor, runtype\n",
    "    elem_type, num_elem_vert, num_elem_hor, runtype = Eltypes, vert, hor, myruntype\n",
    "  \n",
    "    CRED, CEND = '\\033[91m',  '\\033[0m'\n",
    "    print(CRED + 'You selected to run a model with: \\n {} vertical elements \\n {} horizontal elements, \\n of type {}\\n with output to {}!'.format(vert, hor, Eltypes, runtype) + CEND)\n",
    "    return \n",
    "\n",
    "w2 = interactive(select_props1, \n",
    "                 vert=widgets.IntSlider(value=6, min=2, max=20, step=1, description='Vert elem:', orientation='vertical', continuous_update=False),\n",
    "                 hor=widgets.IntSlider(value=1, min=1, max=10, step=1, description='Hor elem:', continuous_update=False),\n",
    "                 Eltypes = widgets.Select( options=[ 'C3D8', 'C3D8R', 'C3D20', 'C3D20R'], value='C3D8R', description='Eltypes:'),\n",
    "                 myruntype = widgets.Select( options=['write in csv file', 'open in Abaqus'], value='write in csv file', description='Runtype')\n",
    "                )\n",
    "wv = w2.children\n",
    "\n",
    "layout_container_eingabe = widgets.Layout(display='flex', flex_flow='row', align_items='stretch', ustify_content='space-between', margin='10px 0px')\n",
    "box_ausgabe2   = widgets.VBox([wv[-1]])\n",
    "box_parameter2 = widgets.VBox(wv[:-1], layout=layout_container_eingabe)\n",
    "wv2 = widgets.VBox([widgets.HTML(value='<b>Parameter:</b>'),box_parameter2,widgets.HTML(value='<b>Ausgabe:<br \\></b>'),box_ausgabe2])"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "d33132b1-3c4d-420b-8cf2-192ea42a6fa8",
   "metadata": {
    "tags": []
   },
   "outputs": [],
   "source": [
    "display(wv2)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "5c8c8700-863b-44b8-9ec5-20dbc8b21cd3",
   "metadata": {
    "tags": []
   },
   "outputs": [],
   "source": [
    "# Set execution mode\n",
    "exec_mode = 'noGUI'\n",
    "if runtype == 'open in Abaqus':     exec_mode = 'script' # open GUI\n",
    "\n",
    "# Set paths and file names\n",
    "caepath = modulespath / 'Beam_Model.cae'\n",
    "filepath1 = modulespath / 'Create_Beam_Model.py' # path to the script to create the beam model\n",
    "job_name = 'Job-Beam' # name of the job\n",
    "\n",
    "# Create string of inputs\n",
    "keys=list(inputdata.keys())\n",
    "createmodel_inputs = \" -- \" + str(\" \".join([str(modulespath),  str(caepath)] + [str(inputdata[mykeys]) for mykeys in keys[:-3]] + [str(num_elem_vert), str(num_elem_hor), elem_type, job_name, str(resultspath), exec_mode]))\n",
    "# Run script\n",
    "output = run(\"abaqus cae \" + exec_mode + \"=\" + str(filepath1) + createmodel_inputs, stdout=PIPE, stderr=PIPE, universal_newlines=True, shell=True)\n",
    "\n",
    "# Show results\n",
    "if exec_mode == 'noGUI':\n",
    "    output = ast.literal_eval(output.stdout.splitlines()[-1]) # convert string representation into list\n",
    "    suplib.display_beam_results(output, ux_t, uz_t) # display results\n",
    "    "
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3 (ipykernel)",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.8.10"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 5
}
